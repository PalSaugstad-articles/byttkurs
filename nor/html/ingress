# Bytt kurs

---------

Disse punktene mener jeg er helt sentrale erkjennelser vi er nødt til
å ta innover oss dersom vi skal å forstå de økonomiske problemene
som verden står overfor, og hvilke spor vi må lete langs for å komme oss videre.

* Det verden nå prøver på er umulig: Stadig økonomisk vekst
* Hvis man prøver å få til noe som er umulig, havner man i trøbbel
* Flere ting tyder på at mange deler av verden etter hvert har kommet til grensen
  der fortsatt økonomisk vekst ikke lenger er mulig
* Banksystemet og finansierings-systemet som vi nå har tåler ikke at vi ikke har økonomisk vekst
* Det er mulig å reformere bankene og finansierings-systemet slik at det også fungerer
  dersom vi har null-vekst og til og med en svak økonomisk negativ vekst.
* Et slik system vil fungere både i områder der økonomisk vekst er ønskelig
  og i områder der svak økonomisk negativ vekst er ønskelig, samtidig.
* Å få til svak økonomisk negativ vekst kan se ut som nærmest umulig, men la oss sammenlikne
  __a__: Få til økonomisk vekst i det uendelige og __b__: få til svak økonomisk negativ vekst uten at
  hele samfunnet kollapser. __b__ er det vi må strebe etter fordi
  __a__ er komplett umulig, mens __b__ "bare" er veldig, veldig, vanskelig
* Vi mennesker har to sentrale egenskaper som nå er i konflikt med hverandre.
  Vi liker å konkurrere, og vi liker å samarbeide. Hele den økonomiske filosofien er bygd
  rundt vår higen etter å konkurrere.
  Vi må endre økonomisk filosofi slik at vår trang til å samarbeide er den som blir dominerende
  av disse to kreftene.

Disse standpunktene er mer detaljert belyst [her][egen].

[egen]: show?f=articles/parsed/byttkurs/nor/byttkurs.pdf
