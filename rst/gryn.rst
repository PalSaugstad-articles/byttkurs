Gryn, normaliserte penger
=========================

Uansett hva slags pengesystem som gjelder, så finnes det en fellesnevner,
nemlig at pengemengden er begrenset på en eller annen måte. I mange
pengesystemer så er ikke denne grensen statisk, det kan tenkes at
pengemengden øker eller minker over tid.

Hvis penger knyttes opp mot en konkret forekomst av noe,
f. eks. gull eller sølv, så vil mengden gull eller sølv være begrensningen.
Da vil pengemengden kunne øke rykkvis, ved at noen oppdager en ny forekomst
for så å utvinne dette. For dagen pengesystem øker pengemengden som følge
av kreditt gitt av bankene.

Det kan være ønskelig å kunne gjøre sammenlikninger på tvers av tid og sted
og pengesystem. Det kan man få til ved å innføre normaliserte penger,
her kalt gryn.

Dette var en idé jeg fikk for endel år siden [#99]_.
For alt jeg vet så har en slik fiktiv valuta vært beskrevet tidligere,
men jeg har ikke funnet noen referanser til slike,
eller sett at denne måten å tenke på har vært brukt innen makro-økonomiske betrakninger.
Jeg har brukt statistikk for Norge fra ca. 1910 og beregnet slike ting som
gryn-justerte renter på innskudd, kontanter og lån, gryn-justert gjennomsnittslønn,
gryn-justert BNP osv.
Jeg har tenkt å flette inn kurver over slikt etterhvert.
Et annet interessant aspekt er dette med at man ikke kan måle noe uten å påvirke det.
Det å måle ting i gryn, for eksempel valutakurser mot hverandre, vil kunne påvirke
hvordan valutakursene beveger seg.

La oss rett og slett definere en fiktiv penge-enhet eller kvasi-valuta, Gryn.
Egenskapen til gryn er at det er akkurat så mange gryn på konto i sum
at hvert menneske i valuatens nedslagsfelt har ett gryn,
eller sagt på en annen måte:
Antall gryn i hvert valutaområde er det samme som antall mennesker i samme område.
Her bruker jeg betegnelsen G-NOK, G-EUR osv. for å angi at det dreier seg om gryn
og hvilken valuta den er knyttet opp mot.

En annen størrelse, omløpshastigheten til penger, bør ideelt sett også beregnes
inn i formelen til gryn.
Det skyldes at omløpshastigheten har betydning for hvor høyt prisnivået
på varer og tjenester er.
Faktorer som kan påvirke omløpshastigheten kan være hvor ofte lønnsutbetainger skjer.
Hvis vi sammenlikner to systemer der månedslønnen er lik, men i det ene utbetales
lønnen én gang pr. måned mens i det andre halve lønnen to ganger i måneden.
I det siste tilfellet kan pengene "rekke rundt" oftere enn i det første,
og dette vil gi en tendens til økte priser fordi det er mer penger tilgjengelig.
Nå beveger jeg meg langt inn i et økonomisk område som kalles kvantitetsteorien.
De fleste økonomer er enige i at kvantitetsteorien gir mening på lang sikt,
altså at det er en klar sammenheng mellom pengemengden og prisnivå på hvis
vi betrakter endringer over lang tid (mange år).
Det som det er uenighet om er om kvantitetsteorien gir noen mening hvis man
analyserer lønns- og pris-endringer på kort sikt. Litt av uenigheten skyldes
høna og egget-prinsippet. Er det slik at lønninger og priser øker fordi
pengemengden øker, eller er det slik at pengemengden øker fordi lønninger
og priser øker?
Av den grunn er det sikkert mange økonomer som vil totalt avvise at gryn,
normaliserte penger har noe for seg i det hele tatt.
Ikke minst på grunn av omløpshastigheten til penger som er vanskelig å beregne
og dermed finne ut hvordan endrer seg gjennom tidene og på tvers av områdene.
Men i og med at jeg ikke er økonom
synes jeg det kunne vært interessant å finne ut i praksis
(ved å sette sammen statistikker over tid og sted) om gryn kan være til hjelp
for å finne sammenhenger i de store linjene i økonomien.
Nedenfor "glemmer" jeg derfor begrepet omløpshastighet, dvs. jeg antar at
omløpshastigheten til penger er konstant.

Det å regne på i en fiktiv gryn-valuta, forutsetter heller ikke at man har tatt stilling til
"høna og egget". Man bare konstaterer at nå har pengemengen blitt det den har blitt, og regner
dermed ut en vekslingskurs mellom gryn og virkelig valuta.

Deretter kan man lage oversikter over trendene i hver enkelt valuta
som en funksjon av hvordan Gryn-kursen endrer med tiden i forhold
til egen valuta. Mellom valutaområder kan man så sammenlikne
gryn-kursene i stedet for de reelle kursene.

Hvis man ser i litteraturen på historiske trender mellom utviklingen av
kurser mellom store valutaer,
vil man se at økonomene er noe i villrede når det gjelder hvordan disse
trendene skal forklares. Ett eksempel er kursutviklingen mellom USD og
DEM/EUR fra 1980 til 2004 angitt i Globale Penger [#GlobalePenger]_ side 55.
Både DEM og EUR svekket og styrket seg på en måte som det ikke finnes
noen gode teorier for.

For å sitere fra boka:

   *Vi kommer ikke utenom å måtte konstatere det ubehagelige:
   Økonomisk teori kan vanskelig forklare dollarens vekst og fall
   gjennom de ubegripelige 1980-årene.*

En ting som er sikkert når det gjelder kursutviklingen mellom valuter er at
det er flere ting som påvirker utviklingen. Man kan i alle fall peke på tre
hovedtrekk som hver for seg kan endre seg på forskjellig måte
i de forskjellige valutaområdene:

- Import/eksport-balansen
- Den generelle økonomiske utviklingen (økonomisk vekst)
- Utviklingen i pengemengden

I en kurve som kun viser kursutviklingen mellom valutaer vil alle disse
tre fenomenene være slått sammen.
Man kan i alle fall eliminere ett av dem ved å innføre Gryn.
Da kan man i tillegg anskueliggjøre ett annet av dem på en ny måte,
nemlig økonomisk vekst, som nå blir hvor mye billigere med tiden varer
og tjenester blir uttrykt i Gryn ved positiv økonomisk vekst.

Hvis vi vet summen av pengemengden og antall mennesker til enhver tid,
kan vi regne ut en gryn-kurs, dvs. antall Kroner eller Euro osv. man kunne ha vekslet mot ett gryn.
La oss for enkelt hets skyld anta at størrelsen M2 som www.ssb.no holder statistikk over,
representerer pengemengden.
Folketallet P blir også lagt i statistikker av SSB. Dermed kan man beregne kursen NOK/G-NOK som M2/P.
For 1. januar 2014 har vi *M2 = 1 894 916 millioner NOK* og *P = 5 109 056*.
Da blir kursen *NOK/G-NOK = 370 894*.
Ett gryn tilsvarte altså 370 894 kroner.
Det betyr at hvis du hadde mer enn 370 894 kroner på kontoene dine i januar 2014,
så hadde du mer enn "din andel" av pengemengden.
(Mye mer faktisk, for man må ta i betrakning at pengemengden er fordelt på husholdningene (ca. 55%)
samt ikke-finansielle foretak (ca. 31%) og kommuneforvaltningen).

Måle valutaers styrke i forhold til hverandre
---------------------------------------------

Valutakurser endrer seg jo over tid av mange forskjellige årsaker.
Behovet for i det hele tatt veksle mellom forskjellige valutaer skyldes jo at forskjellige
valuta-områder handler med hverandre, så endringer i valutakursene kommer som en reaksjon
på endringer i samhandelen med varer, tjenester og finansielle transaksjoner.

Når to forholdsvis like valutaområder handler med hverandre over lang tid,
så vil den valutakursen som etablerer seg også være avhengig av pengemengdene
og befolkningene i disse valutaområdene.
Hvis f. eks. landene vi betrakter er Sverige og Danmark, så kan det være interessant
og finne ut hvordan gryn-kursene i de to landene harmonerer. Ideelt sett burde vekslingsforholdet
i gryn være 1 til 1.
Foreløpig har jeg ikke det statistiske grunnlaget for pengemengder og befolkning i Sverige
og Danmark, så det for jeg komme tilbake til senere.

.. Statistikk over dansk og svensk pengemengde [#99]_


Måle formuer
------------

En milliardær i Norge i 2014 har altså bokførte verdier for *2696 Gryn* eller mer,
en millionær har verdier for 2,7 Gryn *(1 000 000 / 370 894)*.

Her er vi inne på noe som er fordelaktig med gryn:
En krone-millionær i 2014 og en krone-millionær i 1980 er to vidt forskjellige ting:
Gryn-kursen NOK/G-NOK i januar 1980 var 37 330 NOK/G-NOK,
så en millionær i 1980 hadde verdier for hele *27 Gryn*,
altså omtrent 10 ganger så mye som en millionær i dag.
Hvis man vil sammenlikne formuer både på tvers av region og mellom tids-epoker,
så vil en gode målestokker kunne være en 1-gryning,
en 1000-gryning
og en million-gryning.
En 1000-gryning vil da tilsvare dagens mange-millionær i dollar og en
million-gryning vil være forholdsvis styrtrik, for å si det sånn.

I et stort valutaområde (mer enn 1 million personer) vil en 1000-gryning
ha såpass liten del av hele kaka at man ikke trenger tenke på størrelsen
på området.
For million-gryning vil man kanskje måtte ta hensyn til at de 1 mill. grynene
må komme fra resten av befolkningen. Formelen under viser faktoren man må
modifisere med. *P* er folketallet og *g* er formuen i gryn. Vi ser
at modifisert formue g\ :superscript:`m` går mot uendelig når formuen går går mot antall
mennesker, naturlig nok siden da har ingen andre noen gryn igjen.

.. math::

    g^m = g  \frac{P - 1}{P - g}


Obs! [#99]_ Formue er jo ikke bare penger, når man måler formuer til folk
så består den av både penger og real-verdier som måles i penge-enheter.
Og hvis alle realverdier hadde vært belånt, og man regner formue som
(real-formue + pengeformue - pengegjeld) så blir det jo sånn cirka like mange gryn i formue
som det er mennesker i valutaområdet, men slik er det jo ikke helt.

Andre enden av skalaen, fattigdom "one dollar a day", kan gryn brukes til dette? [#99]_

For å sette det i perspektiv så stakk Kjell Inge Røkke av med formue-seieren
i Norge i 2014 med 11,8 milliarder.
Det er "bare" 32000 Gryn,
så han må skaffe seg 32 ganger så mye formue før han kan kalle seg million-gryning.
Bill Gates $86 Billions, dvs. $86 000 000 000,
dvs. NOK 668 000 000 000, dvs. at han er omtrent dobbel million-gryning.
Dette er under forutsetning at valutakursen G-NOK / G-USD er omtrent 1. (feil! se over [#99]_)

Prisutvikling i Gryn
--------------------

Som vi har sett så går gryn-kursen stort sett opp etter som tiden går.
I Norge, etter 1910, så har det bare vært én periode der gryn-kursen har minket,
og det er årene 1921 til 1935.
I disse årene ble det mao. ført en særdeles stram pengepolitikk.
Det man prøvde på i denne perioden (etter 1. verdenskrig) var å komme tilbake til gullstandarden.

KPI, konsumprisindeksen, brukes jo vanligvis når man vil sammenlikne priser til forskjellige tider.
Dessuten brukes KPI til å beregne økonomisk vekst.
BNP jo er uttrykt i kroner, så hvis man ikke hadde justert BNP med KPI,
så ville hoved-tyngden av økonomisk vekst skyldes pengemengde-økningen,
mens det jo er veksten i real-økonomien man ønsker å måle.

Vi er jo vant til at priser på varer og tjenester øker ettersom tiden går.
Dette går parallelt med økningen i pengemengden,
eller utviklingen av gryn-kursen for å bruke den terminologien.
Pris-sammenlikninger i Gryn kan være en alternativ måte å sammenlikne på.
Det man da må være klar over er at prisene på enkeltvarer *synker* med tiden
når de er oppgitt i Gryn dersom vi har økonomisk vekst i perioden.
Grunnen til dette er at økonomisk vekst betyr at flere varer og tjenester blir tilgjengelig,
og siden Gryn pr. definisjon er konstant med tiden,
må varene og tjenestene bli billigere for at man skal "få plass" til flere av dem på budsjettet.
Man kan endog uttrykke KPI i Gryn-justert valuta.
Veksten i økonomien vil da kunne beregnes som minkningen i Gryn-justert KPI.
Dette vil ikke gi eksakt samme kurve for økonomisk vekst
som når man regner på den tradisjonelle metoden,
men kurvene følger hverandre ganske tett,
så begge må kunne sies å gi et nogenlunde riktig bilde på den økonomiske utviklingen.

Se grafiske framstillinger av makroøkonomiske forhold her:
`saugstad.net/statistikker <https://saugstad.net/statistikker>`_
