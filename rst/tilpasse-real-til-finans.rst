Nyliberalisme, hva nå?
======================


Hva er så det nyliberalistiske økonomiske prosjektet?
Vansklig å si, men jeg kan jo fortelle hvordan det fortoner seg for meg.
Verden har kommet i en slags tilstand av voksesmerter,
og vi har en real-økonomi og en bank- og finans-sektor som ikke passer til hverandre lenger.
Før, når økonomisk vekst ikke var å vanskelig å få til,
altså når velstandsutviklingen over store deler av verden ikke hadde gått i metning ennå,
da passet systemene ganske bra sammen.
Bank og finans er konstruert med vekst i bakhodet,
det *var* nødvendig å øke tilgangen til varer og tjeneser til hele verdens befolkning.
Japan hadde en formidabel vekst på 1960- og 1970-tallet.
På 1990-tallet stoppet veksten nesten opp,
og Japan har hatt lav reell vekst helt fram til nå (2015).
Stadig vekk er det likevel økonomer som uttrykker bekymring over at Japan har "stagnert".
Det samme fenomenet ser vi i nye deler av verden.
Økonomer har den rare forestillingen om at hvis kurven som viser prosentvis vekst er flat,
f. eks. 5%, så er økonomien på en måte statisk,
og man regner det som om det skal la seg gjøre å fortsette med samme trend evig.
Det er jo ikke riktig.
Hvis du sammenlikner et samfunn etter at det har hatt 5% vekst i 14 år,
så ser man jo at det er en enorm forskjell.
De materielle godene har blitt doblet i løpet av perioden.
Hvordan kan man i sin villeste fantasi klare å resonere seg fram til
at det er helt naturlig at de neste 14 årene skal gi en ny dobling?
Jeg fatter det ikke.
Problemet er bare (slik som jeg ser det) at man har ikke tenkt å gjøre noe med grunnstrukturen,
bank og finans.
Man er vant til at disse fungerer på et vis,
og vil heller prøve å tilpasse real-økonomien til grunnstrukturen,
enn å se på hva man må endre i grunnstrukturen for at samfunnsutviklingen
kan gå videre *uten* økonomisk vekst!
Naturligvis så er det skremmende å skulle endre på noe man har erfaring med at funker,
men hvor mange hint trenger man egentlig før man innser at en aldri så liten ny-tenkning må til?