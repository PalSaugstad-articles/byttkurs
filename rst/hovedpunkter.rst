Grunn-holdninger
================

De følgende punktene mener jeg er helt sentrale erkjennelser man er nødt til
å ta innover seg dersom man vil prøve å forstå de problemene som verden står
over for, samt hvilke spor vi må lete langs for å komme oss videre.

- Det verden nå prøver på er umulig: Stadig økonomisk vekst
- Hvis man prøver å få til noe som er umulig, havner man i trøbbel
- Flere ting tyder på at mange deler av verden etter hvert har kommet til grensen
  der fortsatt økonomisk vekst ikke lenger er mulig
- Banksystemet og finansierings-systemet som vi nå har tåler ikke at vi ikke har økonomisk vekst
- Det er mulig å reformere bankene og finansierings-systemet slik at det også fungerer
  dersom vi har null-vekst og til og med en svak økonomisk negativ vekst.
- Et slik system vil fungere både i områder der økonomisk vekst er ønskelig
  og i områder der svak økonomisk negativ vekst er ønskelig, samtidig.
- Å få til svak økonomisk negativ vekst kan se ut som nærmest umulig, men la oss sammenlikne
  **a**: Få til økonomisk vekst i det uendelige og **b**: få til svak økonomisk negativ vekst uten at
  hele samfunnet kollapser. **b** er det vi må strebe etter fordi
  **a** er komplett umulig, mens **b** "bare" er veldig, veldig, vanskelig
- Vi mennesker har to sentrale egenskaper som nå er i konflikt med hverandre.
  Vi liker å konkurrere, og vi liker å samarbeide. Hele den økonomiske filosofien er bygd
  rundt vår higen etter å konkurrere.
  Vi må endre økonomisk filosofi slik at vår trang til å samarbeide er den som blir dominerende
  av disse to kreftene.
