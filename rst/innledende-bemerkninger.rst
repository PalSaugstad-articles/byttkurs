Innledende bemerkninger
=======================

**Dette er en tekst som er under arbeid!**

Jeg begynte på selve skrivingen i januar 2015, men tankene bak
har jeg delvis brukt flere år på å tilegne meg.

Noen deler av teksten er bare på tankestadiet ennå, noen av disse tankene har jeg allerde nogenlunde
klart for meg slik at det bare er å skrive dem ned, mens andre viser det seg at dukker opp etterhvert som
jeg skriver. Jeg bruker fotnote [#99]_ på steder jeg vet ikke er ferdige eller løse tanker osv.

.. [#99] Her trengs det opprydding/flytting eller omskrivning

I noen tilfeller i teksten der jeg bare har en punkt-for-punkt-liste, er dette kun en foreløpig huskeliste som
jeg skal omarbeide til en lengre tekst.

.. include:: endringer.rst
