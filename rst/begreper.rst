Økonomisk vekst og andre tilknyttede begreper
=============================================

Hva er økonomisk vekst?
-----------------------

I Økonomimileksikon (Lillebø) er økonomisk vekst definert slik:
"Økning i produksjon eller inntekt."
I samf øk (Sandmo) [#Sandmo]_ er Robert Solow beskrevet som en foregangsfigur ang. moderne vekst-teori.
I dagligtale beskriver økonomisk vekst hvilken grad av mulighet et samfunn har for å øke
produktiviteten, som i sin tur må føre til et økt forbruk (i en lukket økonomi).
Det kan tenkes at økonomer generelt vil anse at denne definisjonen er altfor enkel og at
begrepet ikke lar seg forklare i en kortfattet framstilling.
Uansett, slik som jeg tolker det de fleste politikere mener med økonomisk vekst,
er at vi må stadig produsere mer varer og tjenester.
Ut fra konklusjonene i perspektivmeldingen som Stoltenberg II-regjeringen la fram i 2013 [#99]_
[#StoltenbergII]_
så skal forbruket i 2060 pr. innbygger bli omlag 3 ganger så høyt som det var i 2010.
Dette er ikke en prognose som har framkommet på grunnlag av en velfundert diskusjon i
meldingen om at dette er et fornuftig nivå,
det har rett og slett framkommet som et resultat av forutsetningene for hele meldingen
som er angitt i vedlegg 1 der det forutsettes at veksten fram mot 2060 skal være 3% [#99]_ pr. år.

Det store spørsmålet blir da i hvilken grad det faktisk er ønskelig at et samfunn oppnår
økonomisk vekst.
Dette er det sagt og skrevet mye om.
Hovedprosjektet til de fleste politiske hovedretningene i Norge og verden for øvrig
er å være i en tilstand av økonomisk vekst,
altså at produktiviteten og forbruket av varer og tjenester pr. innbygger hele tiden skal bli høyere.
Det kan virke som at hele økonomifagets bærebjelke er å legge til rette for stadig høyere
produksjon og større forbruk.
Dette uansett hvilket forbruksnivå samfunnet allerede har oppnådd.

Produktivitet og antall arbeidsplasser
--------------------------------------

For meg virker det som at de fleste politikere forbinder økonomisk vekst med det å øke antall
arbeidsplasser.
Det er i utgangspunktet en naturlig slutning å trekke, men det er jo feil at økt vekst automatisk
gir flere arbeidsplasser.
Faktoren produktivitet må også tas med i regnestykket.

- Man kan øke antall arbeidsplasser ved å øke produksjonen og forbruket forutsatt at produktiviteten
  ikke øker eller øker mindre enn produksjonsøkningen.
- Man kan også øke antall arbeidsplasser ved å senke produktiviteten forutsatt at forbruket ikke senkes
  eller senkes mindre enn senkningen av produktiviteten

Utvikling
---------

Utvikling, alstå det å finne på nye produkter og tjenester blir av mange også hardt bundet mot
økonomisk vekst.
"Man kan jo ikke ha utvikling uten økonomisk vekst".
Det er jo feil!
Økonomisk vekst på den ene side og utvikling er jo to helt uavhengige parametere i samfunnet.
Man kan godt tenke seg økonomisk vekst uten utvikling.
Det vil jo si at man ikke finner opp noen nytt, men alt man gjør det gjør man hele tiden mere av.
Man får seg større hus, større biler, lager mer veier og jernbaner, tilbyr flere flyreiser,
flere turist-destinasjoner, flere kjøpesentre osv osv.

Man kan også tenke seg utvikling uten økonomisk vekst:
Det vil si at man finner på noe nytt og begynner dermed å gjøre nye ting eller gamle ting på nye måter,
men samtidig så slutter man å gjøre noen av de gamle tingene eller gjør mindre av de gamle tingene.

Dele samfunnsmaskineriet i bestand-deler
----------------------------------------

- Bankvesen
- Realøkonomi
- Finansvesen

Man kan dele samfunnsmaskineriet i tre hovedbestand-deler.
Bankvesen og finansvesen utøves nå tildels av de samme institusjonene, bankene.
Likevel kan man skille dem:
Bankvesen handler om sentralbanken, kontopenger, pengesedler, kreditt (lån) til publikum.
Finansvesenet driver med investeringer av formuer bl. a. via aksjer.
Real-økonomien er all handel med varer og tjenester og framstillingen av disse (jobbene)
når man ser bort fra pengetransaksjoner.
Så man kan si at real-økonomien er det vi ønsker å holde på med,
bank og finans er der kun for å samordne og måle realøkonomien.
Hvis alle hadde vært snille, og ikke tatt mer av lasset enn de trengte,
og jobbet selvom de ikke fikk penger for det, så hadde vi ikke trengt bank og finans,
men real-økonomien trenger vi!
Med real-økonomisk vekst (til vanlig kallt økonomisk vekst)
så mener vi at de reelle tingene faktisk har blitt større eller flere,
f. eks. at vi bor i større hus nå enn før,
mens det at prisene øker ikke er økonomisk vekst i seg selv, det er inflasjon.

Knytte bankvesen til Rasjonell materie og finansvesen til Emosjonell materie i
"Det 5. trinn-kontekst"? [#99]_

Økt forbruk, hvor lenge er det mulig å fortsette med det?
---------------------------------------------------------

La meg presentere en økonomisk modell, en forenkling av hvordan et samfunn fungerer.
I modellen ser vi bort fra slike ting som at ressursgrunnlaget i verden er endelig.
Vi antar også at folketallet er konstant.
Vi observerer samfunnet over mange år.
Det at ressursgrunnlaget ikke er endelig,
betyr at det (i modellen) ikke er noen grense for hvor mye olje, kull, gull, jord, sand,
osv. osv. vi kan forbruke.
Det er heller ingen øvre grense for hvor mye CO\ :sub:`2` vi kan generere eller hvor mye søppel vi kan produsere.
Det er heller ikke mulig å ødelegge naturen eller forårsake utryddelse av arter i modellen.
Modellen tar heller ikke for seg hvordan aktiviteter skal finansieres,
vi ser på en rent realøkonomisk modell, vi antar alså bare snille mennesker,
og at bank og finans ikke eksisterer, men at samfunnet likevel fungerer.
Utvikling av nye tjenester og produkter og produktivitetsøkning er mulig i modellen.
Dette fordi det jo nettopp er økonomisk vekst jeg ønsker å belyse,
og selvom ikke utvikling er en nødvendig faktor for å få til økonomisk vekst,
så er f. eks. en forbedring av produksjonen slik at man kan klare å produsere mer med på å drive økonomien videre.

Grunnen til at modellen kan være relevant selv med så grove forenklinger,
er at vi jo ønsker et visst forbruk av varer og tjenester,
og i så fall så må vi jo produsere dem.
Det bank og finans gjør, er å legge til et regelverk oppå real-økonomien som den kan fungere i.
Man kan like gjerne tenke seg helt andre mekanismer, som at folk har lyst til å jobbe,
og at de ikke har lyst til å forbruke mer enn en eller annen moralsk norm som vi ikke trenger
å beskrive nærmere, sier at de bør gjøre.

La oss tenke oss at vi er på et gitt nivå når det gjelder produksjon og forbruk i modellen
og at den fortsetter å være konstant over mange år.
I fra et visst år bestemmer dette samfunnet seg for at man vil ha (real)-økonomisk vekst.
Man produserer altså gradvis flere varer og tjenester,
variasjonen i det som tilbys av varer og tjenester går opp
og forbruket går opp.
Man kan altså forurense så mye man vil
og hente ut så mye natur-resurser man vil.

Hvis det finnes noen faktor i denne modellen
som setter en øvre grense for hvor mye som kan produseres og forbrukes,
hva er den i så fall?

I følge alle økonomer jeg har lest bøker av, så finnes det ikke noen øvre grense for hvor høyt
forbruket vil bli under disse gitte betingelsene.
Selv ikke Herman E. Daly mener det. I [#DalyNodvendighetensOkonomi]_ sier han
på side 125 i den danske oversettelsen:
"Hvis økonomien voksede i et ubegrænset tomrom, ville den ikke kollidere med noget,
og dens vækst ville ikke være forbundet med mulighedsomkostninger."
Daly mener at begrensningen for økonomisk vekst ligger i at vi lever i en endelig
verden, og at det er uttømming av ressurser som er begrensningen.

Jeg mener at vi i tillegg til dette har vi en annen begrensing.
Vi kan si det slik at det finnes en ytre begrensning (en endelig klode) og
en indre begrensning (hvor mye et menneske klarer å rekke over i løpet av et
gitt tidsrom).

Begrensningen er tid.
Hvert individ har et visst antall timer i døgnet som det klarer å utnytte.
Ettersom forbruket øker, minker nødvendigvis død-tiden,
altså tiden man bare sitter å kikker ut i lufta.
Man gjorde mer av det før i tiden enn nå, rett og slett fordi det ikke var så mye å finne på.
Alternativt, hvis produksjonen er tilstrekkelig tungvind,
så må man bruke mer av tiden til å lage tingene,
og har desto mindre tid i døgnet til å forbruke dem.

I modellen kan vi tenke oss at produksjonen blir så effektiv
at alle har 16 timer til rådighet i døgnet til å drive med aktivt forbruk.
Etterhvert som tilbudet av varer og tjenester øker,
må man stadig bruke mer tid på reising, gå på helsestudio, vedlikeholde huset, gå på konserter,
gå på kjøpesenteret for å handle osv.
For hvis man ikke gjør det, altså velger å slappe av i stedet for aktivt å bedrive forbruk,
da er det ikke lenger noe grunnlag for å fortsette med å øke produksjonen,
ergo stanser den økonomiske veksten opp.

For å bruke økonomiske termer så er det jeg diskuterer her marginal grensenytte,
og at pga. den indre begrensningen på hvor mye vi som mennesker klarer å absorbere av inntrykk,
så vil tilslutt enda et inntrykk i form av å benytte en tjeneste eller en gjenstand
ikke føles nyttig lenger, det blir mer en plage enn en nytelse.

Man kan vanskelig tenke seg at den økonomiske veksten fortsetter.
I stedet får vi et permanent konstant men høyere forbruk enn vi hadde de
første årene vi observerte modellen, altså før den økonomiske veksten startet.
Det er riktignok ikke helt umulig å tenke seg fortsatt real-økonomisk vekst.
Vi kan tenke oss at vi lager roboter som blir så intelligente
at selv robotene synes de har behov for et visst forbruk.
Dermed kan vi fortsette med en industri
som både produserer og betjener "behovene" til disse robotene,
men hva er vitsen med det, da?
Ønsker vi et slikt samfunn?
Jeg tviler på det.
Vi kan også tenke oss at vi begynner å overdimensjonere alle ting,
f. eks. bytter ut alle biler med større og tyngre biler med det til hensikt
at vi ønsker å ha økonomisk vekst
uten at det nødvendigvis betyr at vi trenger å legge ned flere timer på å forbruke dem.
Tilslutt kan vi tenke oss at bilene blir upraktisk store,
og at hvis vi likevel ønsker å fortsette med dette,
så blir det mer en tvangshandlig for å få til stadig vekst
enn det blir fordi vi egentlig ønsker det.
Antall biler pr. person er også en mulighet.
Man kan klare å eie opptil dusinvis med biler uten at man bruker mer tid på dem totalt,
man fordeler bare tidsbruken mellom dem.
Jeg klarer ikke se for meg at dette kan bli en vanlig måte å leve på heller.
Etter å ha skaffet seg de første 5-6 bilene tror jeg man går litt lei,
grensenytten går mot null.

En innvending kan være at noen mennesker har et vanvittig høyt forbruk og luksus,
og at når noen kan leve slik, så kan vel flere bli ekstremforbrukere.
I den prosessen vil vi trenge vekst, og det er ikke uten videre gitt hvilket nivå
dette vil slutte på. Det kan gå mange år ennå før man kommer dit.
Man kan tenke seg at målet er at alle skal ha hver sitt private jetfly og minst
én luksus-yacht til å farte rundt på de sju hav før vi er "i mål".

Ja, jeg er enig med det at forbruk for enkelt-mennesker tilsynelatende ikke har
noen grenser (båter, hytter, biler osv.) men betrakningene
av dette i makro stiller seg jo litt anderledes:
Lenge før man blir ekstrem-forbruker, så begynner man å etterspørre personlige
tjenester som vaskehjelp, personlig trener, kokketjenester osv,
altså tjenester som krever arbeidstid fra andre.
I så fall vil det jo etterhvert være flere individer å dele det ekstreme forbruket over,
for de som utfører tjenestene kan
jo ikke selv også være ekstrem-forbrukere, da måtte jo i så fall enkelt-mennesket
vi først betraktet utføre slike tjenester for andre.
Eller sagt på en annen måte: I samfunn med ekstremforbrukere må gini-indeksen være høy.
Dette stemmer godt med statistikken tror jeg.
Dermed, i makro kan det godt tenkes at man kan si at det gjennomsnitlinge
forbruket har en endelig øvre grense uavhenging
av om tilgjengelige ressurser er uendelig.
For meg ser det ut som at samfunnets iver etter å etterstrebe økt vekst legger
til rette for ekstrem-forbruk,
men at man kanskje ikke tenker på at dette fører til høyere gini-index og at man dermed
etterhvert ikke klarer å øke veksten i makro mer.

For ikke lenge siden (mars 2015) er det riktignok flere store aktører
(bl. a. OECD og IMF)
som tar til ordet for at for skjev fordeling og ulikhet hemmer vekst.
Det vil nok dessverre ta en god stund ennå før de samme aktørene også lanserer teorier
om at eksponensiell vekst uendelig lenge ikke er mulig,
og at vi ser og har sett tegn til at vi har kommet til punktet
der flere økonomiske områder har kommet til et metningspunkt
og følgelig ikke makter å vokse mer (Japan på 1990-tallet og USA på 2000-tallet, Europa 2010-tallet)

Jeg vil gjerne reflektere litt over dette med at økt ulikhet hemmer vekst.
Dette er jeg helt enig i, og jeg tror at dette er en gjengs oppfatning i flere politiske leire,
helt fra ytterste venstre til ganske langt mot høyre.
Men hva betyr dette?
Jeg vil påstå at dette underbygger tesen om at det finnes en grense for vekst som er gitt av grensen for
hvor mye hvert menneske klarer å konsumere.

Min påstand er at *gitt en målt skjev-fordeling, så kan man oppnå et maksimalt samlet forbruk*.
Dersom man reduserer skjevfordelingen noe, så kan man oppnå et høyere maksimalt samlet forbruk.
Dette skyldes at de svakeste gruppene som får mer midler til å etterspørre varer og tjenester vil gjøre
dette, mens de som er best stilt vil beholde sitt forbruk.
I og med at produksjons-kapasiteten ikke er problemet,
(man har nok av arbeidskraft, og nok av ressurser),
vil den økte etterspørselen faktisk føre til høyere produksjon.

Er dette da det samme som det IMF og OECD hevder?
Ikke helt, bare nesten.
For de sier bare at *økt* ulikhet *hemmer* vekst.
Altså sier de ikke at det er umulig å få høyere forbruk enn en maksimal verdi ved en gitt skjev-fordeling,
bare at det er vanskelig.

IMF og OECD sine uttalelser kan dermed ses på som deres forklaring på at veksten i verden stagnerer,
nemlig at der man observerer økt ulikhet så stagnerer veksten fordi de fattigste etterspør mindre
mens de rikeste ikke klarer å etterspørre mer enn at det samlede forbruket likevel holder seg konstant.

Men hvis det er slik (og dette vet jeg på det nåværende tidspunktet ikke) at man har observert at økt
ulikhet faktisk har senket det totale forbruket,
så vil jeg si dette er en sterk indikator på at min teori har noe for seg.

Det er også tegn i tiden som må virke svært skremmende på mainstream-økonomer:
Mange steder rapporteres det om at unge mennesker ikke i like stor grad som generasjonen
før dem etterspør store boliger, biler og andre forbruksvarer.
Slike mennesker kan (ifølge mainstream-økonomer) ikke vite sitt eget beste.
Det kan jo på lengre sikt føre til negativ økonomisk vekst!
Her må vi nok legge mye av skylden på smart-telefoner.
Unge folk vil helst sitte med nesa si ned i sosiale medier,
og har dermed ikke tid til overs til å kjøre sin egen bil,
de vil heller sitte på en T-bane eller noe.
Man ser altså tendenser til at folks disponering av tid tenderer til å dreie
seg om aktiviteter som i liten grad fører til økt forbruk.
Delings-økonomien er jo også en nøtt økonomisk sett.
Dersom både bilkjøring reduseres og hver bil får en høyere årlig kjørelengde,
vi jo dette kunne gå ut over bilindustrien siden det totale antallet biler
vil gå ned. Foreløpig er jo dette ikke noe problem,
det er nok av områder i verden som ikke har høy biltetthet enda.
På lengre sikt, derimot, gitt stabil folkemengde, tenderer dette til at vi
kan gå mot nullvekst eller negativ vekst innen bilindustrien.

Innspill ut fra hva som står om holdbarhet av produkter i 100-årsmålene [#99]_ [#Andersen100]_
Det vil være en fordel om ting gjøres mer holdbare. Da kan vi leve like komfortabelt som nå
selvom resursbruken kan gå ned.
Vi må da huske på at en overgang fra produkter med kort varighet til produkter med
lenger holdbarhet uten økning i antall ting gir negativ økonomisk vekst,
fordi man i mindre grad enn før da trenger å arbeide for den samme komforten.
Dette er jo også gode nyheter:
Negativ økonomisk vekst er ikke nødvendigvis det samme som lavere komfort!

Modellen inneholder ikke bank og finans,
så vekstmuligheter innen disse sektorene vil ikke bli oppdaget med denne forenklingen.
Vi har ved flere anledninger sett vekst innen bank og finans.
Et eksempel er amerikansk bank og finans som på begynnelsen av 2000-tallet ble mer kreative
når det gjelder å sette sammen forskjellige låneprodukter til nye finansieringspakker,
samt at bankene lånte ut penger til stadig flere grupper,
deriblant såkalte sub-prime lån til lånetakere som ville inn på boligmarkedet
uten at de egentlig hadde økonomi til å betjene lånene.
Teorien var at ingen likevel kom til å tape penger,
for lånebetingelsene var slik at låntakeren kunne kvitte seg med hele rest-lånet
ved å overlate huset til banken, samt at alle visste at boligmarkedet bare steg og steg.
Når boligmarkedet ikke steg likevel gikk jo dette veldig galt,
med smitteeffekter helt til de såkallte "Terra-kommunene" i Norge.
Det viser seg vel at på lang sikt kan man ikke få til reell varig vekst innen bank og finans.
Hvis man prøver, vil det typisk utvikle seg en boble som sprekker
med alle de uheldige virkningene dette fører med seg.

Vekst innen finans, sub-prime, Terra

Metningspunkt er et stikkord her:
Tilbud og etterspørsel er styrt av flere ting, en av dem er metningspunktet

.. [#Sandmo] Agnar Sandmo - Samfunnsøkonomi, 2006
.. [#StoltenbergII] Stoltenberg II - Perspektivmeldingen 2013 - http://www.regjeringen.no/nb/dokumenter/meld-st-12-20122013/id714050/
.. [#Andersen5] Dag Andersen, Det 5. trinn, 1. utgave 2004
.. [#Andersen100] Dag Andersen (Red), 100-årsmålene, 2009
.. [#DalyNodvendighetensOkonomi] Herman E. Daly, Nødvendighetens økonomi, 2008
