
.. [#]  Her er en e-post som jeg sendte til Per Hjalmar Svae (foredragsholder og forfatteren av boka "Grønn økonomi")
        og Øyvind Solum, MDG Nesodden og arrangør av foredraget med Per Hjalmar Svae på Nesodden 29. januar 2014.
        
        Jeg presenterte her noen synspunkter ang. temaer som vi hadde diskutert i løpet av foredraget, og kom også med
        noen innspill som ikke var tema på foredraget.
        
        
        :Dato: 12. mars 2014 07:45
        :Fra: Per Hjalmar Svae
        :Emne: SV: Charles Eisenstein
        
        Hei!
        
        Tusen takk for interessant e-post.  
        
        Venleg helsing
        
        Per Hjalmar Svae
        
        :Opprinnelig melding:
        :Fra: Pål Saugstad [mailto:pal@saugstad.net] 
        :Sendt: 11. mars 2014 23:34
        :Til: Per Hjalmar Svae
        :Kopi: Øyvind Solum
        :Emne: Charles Eisenstein
        
.. [#]
        Hei
        
        Jeg har lest boka til Charles Eisenstein 'Sacred Economics'. Mye interessant der, men heller ikke han diskuterer
        de vesentlige aspektene ved pengesytemene, synes jeg. I appendix på side 447 står vel litt av det som jeg synes
        en slik bok bør starte med, nemlig hvordan skal vi forstå penger, er det riktig å si at bankene skaper nye penger
        ut av ingenting hver gang de låner ut penger, eller er det en bedre modell å si at de samme pengene kan eksistere
        på flere steder samtidig og/eller at de kun eksisterer på de tidspunktene man foretar transaksjoner. 
        Diskusjonen hans rundt pengeystemer av typen "full-reserve banking" 
        kontra "fractional-reserve banking" er interessante. Det var jo i grove trekk et full-reserve-system
        som Silvio Gesell foreslo.
        
        En av tingene jeg savner i boka, var også det jeg nevnte på møtet at jeg kunne tenke meg å spørre om,
        betraktninger rundt størrelsen på K2 i forhold til M2.
        
        I grove trekk er det vel slik i Norge i dag at K2 er dobbelt så stor som M2. Nå som bankene er pålagt å øke sin egenkapital
        gir vel dette som resultat at M2 minker ytterligere i forhold til K2 om jeg ikke tar helt feil.
        (Begge øker jo, men K2 øker mer i % av seg selv enn M2 siden banken må dra inn deler av M2).
        Det betyr jo igjen at rentetrykket på husholdningene blir forverret både fordi bankene må øke "rentemarginen" 
        etter eget utsagn for å bygge opp mer engenkapital og at "rentemarginen" 
        i praksis blir forverret av at forholdet M2/K2 blir ytterligere lavere. 
        Begrepet "rentemargin" må da være svært misvisende? Dersom M2 og K2 hadde vært like store ville det gitt mening,
        la oss si at begge var 2000 milliarder, og forskjellen mellom innskuddsrente og utlånsrente var 3%. 
        da ville bankene ha inntekt på 3% av 2000 milliarder. Men nå er jo M2 er litt under 2000 milliarder og K2 er litt
        større enn 4000 milliarder. La oss si at rentemarginen var 0%, at både innskuddsrente og utlånsrente var 3%, da vil
        jo bankenes inntjening bli 3% av diffen mellom K2 og M2, altså 3% av endel mer enn 2000 milliarder! Men 0% "rentemargin"!
        
        Og altså, styrkingen av bankenes egenkapital øker misforholdet mellom K2 og M2 (tror jeg) og øker dermed sannsynlingheten
        for at lån blir misligholt fordi der blir vanskeligere å få tak i penger til å betale lån med når M2 krymper i forhold til K2,
        er ikke det et paradoks? Er ikke dette tegn på at pengesystemet vårt legger opp til at vi løper rett inn i en ny og værre
        bank-krise?
        
.. [#]
        Og forresten, til diskusjonen om hvor høyt forbruk man kan ha og om vi vil nå et tak på forbruket uavhengig av om det finnes
        uendelig mange ressurser:
        
        Ja, jeg er enig med det at forbruk for enkelt-mennesker ikke har noen grenser (båter, hytter, biler osv.) men betrakningene
        av dette i makro stiller seg jo litt anderledes: Lenge før man blir ekstrem-forbruker, så begynner man å etterspørre personlige
        tjenester som vaskehjelp, personlig trener, kokketjenester osv, altså tjenester som krever arbeidstid fra andre.
        I så fall vil det jo etterhvert være flere individer å dele det ekstreme forbruket over, for de som utfører tjenestene kan
        jo ikke selv også være ekstrem-forbrukere, da måtte jo i så fall enkelt-mennesket vi først betraktet utføre slike tjenester
        for andre. Eller sagt på en annen måte: I samfunn med ekstremforbrukere må gini-indeksen være høy.
        Dette stemmer godt med statistikken tror jeg. 
        Dermed, i makro kan det godt tenkes at man kan si at det gjennomsnitlinge forbruket har en endelig øvre grense uavhenging
        av om tilgjengelige ressurser er uendelig. For meg ser det ut som at samfunnets iver etter å etterstrebe økt vekst legger
        til rette for ekstrem-forbruk, men at man kanskje ikke tenker på at dette fører til høyere gini-index og at man dermed
        etterhvert ikke klarer å øke veksten i makro mer. Etter det jeg har lest, ser det ut som at denne grensen allerede ble
        oppnådd i Japan på 1990-tallet og i USA på 2000-tallet. I Europa skjer det vel omtrent nå.
        Dette er parallelt med at de respektive sentralbankene begynte med kvantitativ lette,
        fordi låne-etterspøreselen og dermed hovedkilden til økt M2 avtar når denne grensen nås.
        Siden pengesystemet ikke er bygd for å takle null-vekst må jo dette bære galt avsted.
        
        Hilsen Pål Saugstad