Gruppering av industri og arbeidsliv
====================================

Det er mange innfallsvinkler her:

- Energi-sektor som egen bransje
- All annen industri

eller

- alt arbeid er egenlig utføring av tjenester

eller

- Energi-sektor
- mat-produksjon
- annen virksomhet
