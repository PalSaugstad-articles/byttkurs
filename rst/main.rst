==============================================
Bytt kurs, økonomisk vekst er ikke bra for oss
==============================================

========= ==========
Av:       |author|
Versjon:  |version|
Revisjon: |revision|
Dato:     |date|
========= ==========

.. include:: revision-header.rst

.. |--| unicode:: U+2013

.. include:: innledende-bemerkninger.rst
.. include:: hovedpunkter.rst
.. include:: bakgrunn.rst
.. include:: begreper.rst
.. include:: tilpasse-real-til-finans.rst
.. include:: hva-er-penger.rst
.. include:: gryn.rst
.. include:: indekser.rst
.. include:: gruppering-av-industri-og-arbeidsliv.rst
.. include:: fond-handelsbalanse.rst
.. include:: skatt-formuesfordeling.rst

.. http://docutils.sourceforge.net/docs/user/rst/quickref.html#title
