Penger og gjeld
===============

- Man kan ikke snakke om penger uten å ta med gjeld
- Penger har fungert forskjellig gjennom tidene

Nødvendige revisjoner
---------------------

- Fra et pengesystem som bare fungerer etter hensikten når vi har økonomisk vekst
  til et pengesystem som fungerer både ved vekst og negativ vekst
- Fra et system som baserer seg på at man sparer i fond (f. eks. pensjon)
  til et system med solidaritet mellom generasjonene der de som er arbeidsføre
  betaler for de som er pensjonister
- Fra et system der gjenstandene, husene, kraftverkene, veiene, infrastruktur betraktes
  som "de bakenforliggende verdiene" for pengene til et system der penger kun forsvares
  med at vi er mennesker som har evner til å skape/omskape våre omgivelser til vår
  egen og andres nytte
- Fra et system der finansiering via fond, aksjer og formuer klusser til tidsbegrepet
  til en mer fra hånd til munn tilnærming

Vårt nåværende pengesystem
--------------------------

Det pengesystemet hele verden opererer med nå kalles et fiat-pengesystem.
Det latinske uttrykket fiat betyr "la det skje",
og grunnlaget i systemet er at man skaper penger etterhvert som innbyggerene
har behov for penger og skaffer seg dem ved å låne penger fra bankene.
Dermed vil det alltid være like mye gjeld som det er penger i systemet.
Dette stemmer når man ser på bankenes samlede balanse:
Det er like mye penger på konto som verdien av gjelden banken har utestående
hos publikum.

Sånn i dagligtale snakker vi gjerne om at bankene låner ut igjen penger som
publikum har satt inn i banken, inntil en viss grense.
I bunnen må bankene en sikkerhet slik at de kan betale ut pengene
som publikum har satt i banken.
Hvis for mange blir usikre på om banken er solid nok,
og pga. denne usikkerhetet tar ut pengene sine av banken
så står banken i fare for å gå konkurs.
Mange mener det er sunt at bankvesenet er bygd slik.
Det at bankene har mulighet for å gå konkurs fører til at de ikke tar unødige
sjanser som så i sin tur er delaktig i at vi får et mer solid bank-system.
Det som kan skje dersom en bank går konkurs er at alle sparepengene går tapt.
Bank-garanti-fondet skal dempe virkningene av dette til en viss grad.

Det som er verd å merke seg her er at vi ikke har noen garanti for at det bare
er noen få banker som går konkurs.
Det er jo ingen katastrofe om en bank som representerer noen prosent av alle
innskytere går konkurs.
Men dersom hele bankvesenet går konkurs samtidig blir det langt verre.
Det er påvist av økonomer at finans-systemet har en selvforsterkende virkning,
se f. eks Norges Bank Skriftserie [#NorgesBankFinansmarkeder]_ side 26 (nr 34, 2004).
Dette gjelder i begge retninger. Dersom økonomien går bra,
slår selvforsterkningen ut ved at det satses for mye slik at vi får en
"overopphetet" økonomi. Dersom økonomien går dårlig så slår selvforsterkningen
ut i at det går dårligere enn det tilsynelatene trenger å gjøre dersom
man analyserer realøkonomien. Dette kan i verste fall slå så negativt ut
at flere store bank-aktører kan gå konkurs samtidig.

Pengesystemet er en viktig bestanddel av hele finanssystemet.
I de følgene avsnittene vil jeg se på forskjellige modeller for hvordan
pengesystemet fungerer for å prøve å belyse denne selvforsterkende virkningen.

.. _Pengemengde: internal-crossref
.. _Kredittindikator: internal-crossref

Makro-størrelsene Pengemengde og Kredittindikator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I Norge holder Statistisk Sentralbyrå oversikt over pengemengden og kredittmengden
til enhver tid.
Slike statistikker utarbeides av de fleste land, men definisjonene og beregningsmetodene
er ikke identiske.
Pengemengden M er grovt sett summen av alle innskudskonti, og kredittmengden K er summen
av alle lån.
Dette er altså makro-økonomiske størrelser.

Pengemengden kan ses på som den mengden med penger som publikum har lånt ut til
bankvesenet slik at bankvesenet kan disponere dem på best mulig måte. Gjennom dette
kan penger sies å ha en dobbelt-funksjon: Det som er formuen til publikum er samtidig
bankvesenets gjeld. Kredittmengden er samme sak i motsatt retning: Det som publikum
har i gjeld er bankvesenets utestående formue.

Man beregner M og K på litt forskjellig vis fra land til land og fra tids-epoke
tids-epoke. I Norge deles pengemengden opp i komponenter: M0, M1 osv. Dvs. nå i disse dager
endres beregnings-metodene enda en gang og det skal nå legges mer vekt på M3, leser jeg
på www.ssb.no/m2 (Publiseringstidspunkt: September 2014).
Dette er begrunnet med at Norges måte å beregne pengemengden på skal harmonisere bedre med
andre europeiske land. Definisjonene skal tilpasses den ECB sine definisjoner.

Men vent nå litt: De fleste som har penger i banken regner dem som sine,
man ser på bank-kontoen som en slags bank-boks der banken oppbevarer pengene for deg,
fordi dette er mye tryggere enn å oppbevare kontanter hjemme.
Dessuten er det veldig upraktisk å leie en bankboks for å oppbevare pengesedler i den,
så vanligvis tenker vi oss bankkontoen som en elektronisk variant av en fysisk bankboks
der pengene våre står. Begrepet pengemengden forsterker jo også dette inntrykket:
Det går tydeligvis an å legge sammen alle innskudd og få en totalverdi på antall kroner
som finnes.
Men hvis dette egentlig bare er penger som er lånt bort til banksystemet,
som så igjen blir lånt ut igjen, så finnes jo egentlig ikke disse pengene likevel, kanskje?

Det er jo nettopp dette dilemmaet som kommer til overflaten i situasjoner med bank-kriser.
Pga. feilslåtte vurderinger i banksystemet, så kan de pengene som man trodde man hadde
trygt plassert i banken plutselig være borte vekk.

Et vesentlig poeng når man prøver å tenke på hva bank-virksomhet egentlig er eller bør være,
er å se for seg at banker ikke er vanlige bedrifter. Bank-skranken/nettbanken er en
barriere (en skranke) mellom real-økonomien og bankvesenet.
Ta f. eks. kontanter: 1000-lapper som ligger i Norges Banks hvelv er egentlig verd null
og nix. Det er bare dekorative kunstverk med noen bokstaver og tall på.
Omtrent som en kølapp som fortsatt er inne i kølapp-maskinen.
Det er først når 1000-lappene dukker opp på den andre siden av skranken, på "vår" side,
at 1000-lappen blir noe mer enn bare et papirark med tall og tegn. Pengesedlene i Norges
Banks hvelv blir da heller ikke regnet med som en del av pengemengden.

Denne barrieren mellom publikum og bank kan også sier å være opphav til begrepene
pengemengde og kredittindikator. Dersom man hadde sett for seg at verdenen bak skranken
var mer likestilt med vår side, ville vel begrepene heller vært publikums gjeld (K) og
bankenes gjeld (M).
En interessant observasjon er at man skulle tro at K og M måtte være like store, fordi
bankene ønkser å låne ut igjen alle pengene de har lånt fra publikum. Faktisk så er K
**større enn** M, bankene har altså lånt ut mer enn de har lånt fra publikum.

Dette synes jeg er et dilemma på linje med det Thomas Piketty beskriver, nemlig at
hvis du summerer alle lands gjeld til hverandre så får du et større tall enn summen
av alle lands tilgodehavende hos hverandre. Piketty beskriver det som at Jorden delvis
eies av planeten Mars ([#Piketty]_ s. 560).

I dokumentet bruker jeg nokså ukritisk begrepene M2 og K2 om pengemengde og
kredittmengde, selvom M3 eller M1 og K1 noen ganger kanskje er mer korrekt. [#99]_
Mange steder bruker jeg de enda mer generelle begrepene M og K,
pengemengde og kredittmengde.

Penger kan være flere steder på en gang-modellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I boken til Erling Røed Larsen, Penger, er en av modellene for hvordan dette pengesystemet
fungerer beskrevet.
Han tar utgangspunkt i at de samme pengene kan være flere steder samtidig [#LarsenPenger]_
(avsnitt "Pengemengen vokser" på side 244),
altså at penger som én person setter inn i banken delvis (f. eks. 90% av dem)
blir lånt ut igjen til en annen kunde selvom beløpet som den første kunden satt inn ikke
blir redusert med 90%.
Dette kan virke litt forvirrende,
for hvis samme kronen kan befinne seg flere steder samtidig,
blir det ikke da veldig vanskelig å holde oversikten over hvor pengene egentlig er?
Jo, det gjør det. Veldig forvirrende. Denne frustrasjonen kan man også lese om i
boka til Charles Eisenstein, Sacred Economics [#EisensteinSacredEconomics]_
i appendix på side 447.
Eisenstein advarer der faktisk mot å påstå at penger *kan* beskrives,
og sammenlikner det med religiøs fundamentalisme dersom man låser seg til at penger
er én bestemt ting eller oppfører seg på én spesiell måte.

.. Bankpenger og private penger [#99]_

Hm, dette er tankevekkende. Er det virkelig slik at det som er fundamentet for nær
sagt alt vi foretar oss gjennom våre liv er noen uforklarlige greier, som det til
og med er farlig å prøve å forklare?
Penger er jo tross alt en konvensjon, eller oppfinnelse, gjort av mennesker,
det er ikke noe natur-gitt ved dem.
Hvis vi ser at systemet ikke funksjonerer etter hensikten, så endrer vi på hvordan
pengesystemet fungerer, det har skjedd mange ganger i historien.
At dette noen ganger har skjedd ved tilfeldigheter, at noen i ett hjørne av verden
har sagt noe, bestemt noe som resten av systemet etter hvert har godtatt,
det er ganske visst.
Eksempelvis dette med "trykking av penger" eller "kvantitativ lette" som det også
kalles.
Dette er nå iferd med å bli en verdensomspennende praksis, selvom endel økonomer
og samfunnsvitere advarer sterk imot det. Det er en motstand der om å innføre dette
nye, men det tvinger seg fra "av seg selv". Av seg selv? Akkurat som det er pengene
som har et indre liv og en egen intellegens som vi mennesker ikke kan påvirke?
Hm og dobbelt hm!

Selvom det kanksje er feil og fundamentalistisk og kan føre galt av sted så tror
jeg det ikke skader i alle fall, og utforske en annen modell enn den nevnt over
hvor pengene er flere steder samtidig.
Et lite tanke-eksperiment skader vel ikke?
Man kan jo bare forkaste hele tankegangen dersom man etter å ha studert den finner
at den overhode ikke stemmer med virkeligheten?
Eller man kan kanskje trekke ut ting man oppdager vha. denne modellen
og bruke dette videre?

Penger skapes i samme øyeblikk som banken låner dem ut-modellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Den modellen jeg tenker på detter mer eller mindre ut av hvordan man i leksikon
kort sammenfatter hva banksystemet handler om:
Banksystemet har en kreditt-skapende evne.
Figur 9.2 i Vidar Ringstad bok Makroøkonomi og norsk stabiliseringspolitikk
[#RingstadMakro]_ viser at økte utlån gir en tilsvarende økning i innskudd.
Også økonomen Anton Hellesøy bruker denne modellen,
han skrev om dette i Klassekampen 7. april 2015.
Jeg har sitert dette i en annen pdf, "sitater.pdf".

En annen artikkel fra Klassekampen som jeg har sitert viser til en rapport
utarbeidet på oppdrag fra den islandske statsministeren ("sitater.pdf").
Her slås det ettertrykkelig fast at modellen beskrevet i forrige avsnitt
er en misforståelse, en ganske utbredt misforståelse.
Her vises også hvorfor modellen som er beskrevet her i dette avsnittet
gir et mye riktigere bilde av hva som skjer. [#IslandskPengeReform]_.

Også Roman Linneberg Eliassen nevner denne betrakningen:
"I 2014 publiserte noen forskere ved Bank of England
|--| overraskende og oppløftende |--|
et dokument hvor de avfeier den tradisjonelle lærebokfremstillingen
av penger og omfavner synet på penger som et resultat av bankenes
utlånsvirksomhet."
[#HysjViRegner]_ s. 95.

Det er en av bankenes hovedoppgaver å yte kreditt overfor kundene.
Vanligvis kaller vi dette for lån, hus-lån f. eks., men kreditt er en bedre betegnelse.
Modellen tar altså utgangpunkt i at man ikke låner det andre folk har satt inn i banken
men at idét banken utsteder lånet,
så skaper den de pengene som du får utbetalt der og da.
Etterhvert, når du betaler tilbake avdragene, så anti-skaper banken pengene igjen.
Du låner altså ikke penger fra andre personers innskudd,
men du låner fra ditt framtidig jeg.
Du får kreditt, rett og slett.
Banken og du lager en avtale som går på at banken skaper penger til deg ut av intet
forutsatt at du lover å betale dem tilbake slik at banken kan ødelegge dem igjen senere.
Hvis vi ser hva som skjer med makrostørrelsene M og K, pengemengden og kredittmengden
eller mengden gjeld, så blir det slik:

  Når du går i banken og tar opp et huslån på 1 million kroner,
  så øker både M og K med 1 million kroner.
  Det som skjer er jo at banken betaler ut lånet til lønnskontoen din eller en annen konto,
  pluss at det blir opprettet en ny konto med negativ saldo som representerer det
  beløpet som du har lånt.

  Når du betaler tilbake et avdrag på lånet ditt på 5000 kroner,
  så minker både M og K med 5000 kroner.

Hva så med det du betaler inn i renter? Rentene på lånet er jo en av tingene bankene tjener
penger på, bankene tar seg altså betalt for den tjenesten de gjør ved å skape penger
ut av løse lufta.
Rentene bruker banken til å betale lønn til sine ansatte osv. så den delen av innbetalingen
blir ikke anti-skapt eller ødelagt slik som avdraget.

Høres det ut som litt juks å kunne ta seg betalt for å knipse med fingeren og bare
putte mer penger inn i systemet og attpå til ta seg betalt for det sier du? Noen vil si
at dette må da være juks og fanteri,
men før man evt. konkluderer med det så må man se på hvilke fordeler og ulemper dette har
for deg og meg, altså hvilken samfunnsnytte dette bidrar med.

Penger omfordeles i samme øyeblikk som banken låner dem ut-modellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ett problem med modellen ovenfor, er at det virker som ren magi at bankene kan
skape penger av intet.
Uttrykket "du låner av ditt fremtidige jeg" er i og for seg snedig,
men er vanskelig å fatte hva som foregår.
Jeg synes generelt sett man bør prøve å unngå situasjoner og beskrivelser av økonomien
som refererer seg fram eller tilbake i tid fordi dette egentlig bare krøller til
det som virkelig skjer.
En ren hånd-til-munn-økonomi stemmer bedre med virkeligheten (real-økonomien);
Du arbeider idag, får lønnen din og går å kjøper ferske varer i butikken.
Et fond der du plasserer formuen din kan sammenliknes med en fryser som du putter mat i.
Det aller meste foregår her og nå.

Så hvordan skal man tenke seg denne modellen?
Banken skaper i og for seg et visst kronebeløp som ikke "fantes" fra før slik som
beskrevet i forrige modell,
men siden penger bare er rasjonell informasjon
(jeg kommer tilbake til dette senere),
så kan vi se på dette som en utvanning av
pengemengden, og dermed at bittelitt "kraft" fra alle pengene som fantes like før
banken skapte disse pengene har blitt flyttet over i de "verdiløse" pengene som banken
skapte.
Dette stemmer jo bra med at ting blir dyrere av at pengemengden øker.
Noe av kraften har forsvunnet ut av alle de "gamle" pengene og over i de nye,
og pr. definisjon har disse nye pengene like mye kraft pr. krone som de "gamle" hadde.
Det stemmer også ganske bra med at bankene premierer de som har penger på konto med
litt av de rentene som de får inn på bakgrunn av låneavtalen som ble inngått.

Når så avdragene blir betalt tilbake og dermed et pengebeløp som tilsvarer avdraget anti-skapes,
føres kraften fra de disse pengene tilbake til alle de eksisterende penger igjen.

Sånn sett kan vi si at utlånsavdelingen i banken er/burde være en gruppe mennesker
som er utnevnt av alle som eier penger og at deres oppgave er å finne gode prosjekter
som egner seg for pengemengdeutvanning.
Man kan tenke seg at for hvert prosjekt burde bankavdelingen sendt ut mail til alle penge-eierene
for å høre om folk var enige i at dette var et prosjekt som de ville være med å støtte
med sin andel av pengekraft-bortfallet. Dette ville jo være helt ugjennomførbart, selvfølgelig,
så istedet har alle som eier penger i realiteten gitt en blankofullmakt til banksystemet om at
pengemengdeutvanningen kan anvendes, men helst med med kløkt og forsiktighet.

Denne modellen baserer seg altså også på avtaler mellom folk, skrevne og uskrevne, og i liten
grad om at pengene finnes på grunn av materielle verdier.

Verdiene "bak" pengene
~~~~~~~~~~~~~~~~~~~~~~

Ofte hører man økonomer og samfunnsvitere si at
"det er de underliggende verdiene som er basis for pengene",
altså at pengene reflekterer de verdiene som finnes i form av hus og veier og produserte
varer osv.
Det stemmer jo ikke helt med den modellene jeg beskrev ovenfor,
hverken den med at krona kan befinne seg på flere steder samtidig, den om at banken skaper
penger ut av løse lufta, eller den at bankene har myndighet til å forflytte kraften i
pengene via utlån.
Det som er saken er jo at pengene eksisterer på bakgrunn av løfter fra de som har lånt dem
om å betale dem tilbake. Det høres jo nesten vakkert ut!
Penger er basert på løfter og avtaler mellom mennesker, ikke hus og ting og slikt!

Riktignok har banken pant i huset, og kan bruke dette som en sikkerhetsventil dersom du
ikke klarer forpliktelsene dine med å betale tilbake lånet.
Slik jeg ser det så er dette bare en nødløsning,
den egentlige avtalen mellom banken og deg er at du har lovet å betale tilbake det
kronebeløpet du har lånt, helt uavhengig av den gjenstanden banken har pant i.

Her er det noen unntak: Pant i en bil følger nåværende eier av bilen uavhengig av
det var en helt annen tidligere eier som avtalte panten.
For huslån har vi et annet unntak, de såkalte sub-prime-lånene som amerikanske banker
ga.
Her kunne eieren levere fra seg huset til banken og dermed være kvitt hele lånet.
Disse lånene oppsto fordi

#. Bankene forestilte seg ikke at verdien på husene kunne synke,
   så de så ikke på det som noen risiko
#. Bankene kunne nå nye kundegrupper på denne måten og låne ut penger nærmest
   uten tanke på at kundene måtte ha en inntekt som sto i samsvar til lånet
#. Siden denne praksisen øker pengemengden som i sin tur (ifølge teorier) øker
   etterspørselen som så igjen øker den økonomiske veksten så kan dette se ut til
   å være et på alle måter formålstjenlig tiltak.

En observasjon slår bein under påstanden om at hus og slikt er underliggende
verdier for penger: Jeg og min familie bor i et hus som vi kjøpte i 1994 for halvannen
million kroner.
Vi tok opp lån, men har betalt dette tilbake for lenge siden.
Vi har heller ingen andre lån
(vel, barna har begynt å bli så store at de har tatt opp studielån, riktignok).
Da må vel det bety at alt vi eier ikke er noen basis for noen penger lenger, eller?
Dersom slike penger finnes må jo det være fordi noen andre har tatt opp større lån
enn det det reelt sett var grunnlag for på sine hus slik at en andel av disse lånene
blir betraktet som basert på vårt hus? Hvis det skulle være sant at det er hus
og gjenstander som er bakgrunn for penger, burde det vært ulovlig ikke å ha lån på
disse gjenstandene.
Det høres ikke ut som noen god idé, men man kan gjenkjenne idéen i form av hva
finansrådgivere og andre sier: Hvis du har ett eller annet verdifullt,
i særlig grad eiendom, så "bør" man ta opp lån (få kreditt) med verdigjenstanden
som sikkerhet slik at man kan utnytte sine verdiers fulle potensiale.
Med kreditten kan man reise på ferie, kjøpe seg seilbåt,
vise at man tenker på miljøet ved å kjøpe seg en Tesla som bil nr. 2 eller 3,
osv.
Det er slik hele systemet legger opp til at man skal tenke, og hvis alle gjør det,
så stemmer det jo at de bakenforliggende verdiene for penger er hus og andre
verdigjenstander, men slik jeg ser det, så er dette er sekundær effekt.
Den primære verdien ligger i det at du har lovet å betale kreditten din tilbake.

Hvordan et godt samfunnsmedlem bør oppføre seg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sånn sett så føler jeg meg som den lite egnet samfunnsmedlem:
Jeg liker ikke å ha lån, jeg ser på det som en nødvendighet fordi man et lite
antall ganger i livet, kankje bare én gang, trenger å kjøpe noe koster så mye
at man trenger å jobbe kanskje flere 10-år for å klare å betale det.
Dermed sitter jeg store deler av livet med mye mer på innskuddskonto enn jeg har
i lån. Antakelig veier ikke de årene jeg hadde lån opp for alle de årene jeg
har og kommer til å få innskudd på konto, for jeg er ikke akkurat flink til å
bruke penger.
Slik som nå f. eks.:
Jeg har tatt meg fri fra jobben noen dager for å skrive dette her, og bruker ikke
stort med penger, bare filosoferer på hva de er.
I stedet burde jeg jo ha dradd verden rundt og opplevd ting, brukt penger og
sørget for at det økonomiske maskineriet går rundt!
Siden jeg har mer på konto enn i lån, og regelen som gjelder er at
"en manns gjeld er en annen manns penger"
("one man's debt is another man's money"),
så betyr det altså at jeg har
veltet byrden med å betaler renter på lån, og ikke minst bekymringer om man
klarer å betale ned lånene over på andre.

Arbeidsdeling og Gesells teorier
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Det er først og fremst på grunn
av arbeidsdelingen vi synes at penger er praktisk. Eller som Silvio Gesell sa det:
"Dersom du har penger i hånden er du kommet halvveis i en byttehandel".
Jeg finner ikke igjen kilden til sitatet, var det ikke Gesell som sa det likevel? [#99]_
Penger er veldig praktisk dersom du har noe eller kan gjøre noe som andre gjerne
vil ha, men de andre ikke har det du vil ha. Da kan man bruke penger som et slags
bevis, en slags kølapp, om at du har gjort ett eller annet som noen andre satt
pris på, for så å vise fram disse pengene til en helt annen person og spørre pent
om han ikke kunne tenke seg å gjøre mine bevis til sine mot at jeg for eksempel
får en kopp kaffe av ham, for det ville jeg sette pris på.

Her trengs det opprydding og grundigere redegjørelse for hva Gesells
teorier var [#Gesell]_ [#99]_.
Det er tross alt disse teoriene som i første rekke fikk meg til å reflektere over
hva penger er.

Kølapper, et spesialtilfelle av penger?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Kølapper i butikker og apoteker o.l. er jo forresten nesten som penger
med noen få unntak.
Kølapper er jo omtrent helt verdiløse i seg selv, det er jo bare en papirlapp med
et tall på. En viktig egenskap med kølapper er jo at det ikke står det samme tallet
på mange kølapper på én gang, og at vi har blitt enige om at den beste kølappen er
den med lavest nummer.
Eller, for å være mer presis, når nummeret hos den som ekspederer stemmer med nummeret
på din lapp, så er den gull verdt.
Men like etterpå, så gidder du ikke ha den engang, du bare kaster den!

Det har utarbeidet seg en norm etter et rettferdighetsprinsipp
som vi sjelden tenker på når vi står der i klyngen og venter på at lappen vår skal
stige i verdi: Vi regner det som selvsagt at nummeret på vår lapp er én høyere
enn på lappen som ble trukket før vår lapp, og vi regner det også som opplagt at
ekspeditøren øker nummeret med akkurat én for hver gang en ny person skal ekspederes.
Hvordan ville vi likt et køsystem etter lotteri-prinsippet, tro? Lappen fra
kølappmaskinen var tilfeldig og nummeret i skranken var også tilfeldig?
Noen ganger ville man hatt flaks og blitt ekspidert ganske fort,
mens andre ganger kanskje ikke i det hele tatt.
Jeg tror ikke jeg ville synes det ville vært noe morsomt.
(Men jeg spiller på Lotto av og til likevel, snakk om å være inkonsekvent!)

Penger er jo ikke så ulikt kølapper, bare at det ikke skal være lavest mulig nummer
på dem, men snarere akkurat passe nummer eller noen ganger høyest mulig nummer.
Når man kjøper hus eksemplelvis er det ikke den som har ventet lengst på å få
kjøpe hus som kommer først i køen.
Det er den som våger å legge kølappen med høyest nummer på bordet som vinner.

Verdien bak penger er arbeid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Betyr det at det må ligge noen verdier til grunn for den "kølappen" med høyest nummer?
Må for eksempel pengene være basert på gull og dermed ha en verdi i seg selv for å
kunne fungere? Må de være basert på hus og veier og ting?
Her igjen trenger vi ikke gå lenger enn til menneskene.
Vi kan si at verdien bak den "kølappen" som du brukte for å kjøpe det huset er det
arbeidet du måtte utføre for å få tak i den "kølappen".
Men hvis du bare gikk i banken og tok ut et lån?
Ikke mye arbeid bak pengene da, sier du?
Nei, men siden du lovet banken å betale tilbake de pengene, så har du jo
også indirekte sagt at du lover å jobbe slik at du får tak i de pengene,
så selvom det arbeidet ikke ligger innbakt i den "kølappen" nå, så gjør de det
likevel i kraft at det løftet du ga.

Dette er gode grunner for at pengene ikke trenger å ha innbakte verdier i form
å kunne innløses mot gull eller at hus og gjenstander trenger å tas i betrakning
som grunnlag for penger, men at det hele kun dreier seg om avtaler mellom mennesker
for å sammenfatte arbeidsmengder inn i et mer universelt begrep slik at man kan
handle med vidt forskjellige gjenstander og tjenester via det samme
begreps-apparatet.

.. Det 5. trinn side 209, kapittel 9,
   Tre ulike materier
   - Den fysiske
   - Den emosjonelle
   - Den rasjonelle

Sett i lys av ideene til Dag Andersen (Det 5. trinn) [#Andersen5]_\ :
Av de tre ulike materiene,
den fysiske, den emosjonelle og den rasjonelle,
så kan penger ses på som rasjonell materie,
på linje med slikt som straffeloven,
menneskerettighetene, informasjonen i telefonkatalogen osv.
(Eksempler på emosjonell materie er drømmer, musikk, dans,
og eksempler på fysisk materie er hus, biler, datamaskiner osv.)
Så siden penger kun er rasjonell informasjon, gjør det at vi kan gi den tilkjenne på de forskjelligste
måter, i form av pregede sølvmynter, notater i en protokoll, bits og bytes på harddisken
til en datamaskin, eller rett og slett i hukommelsen i en eller flere tiltrodde personer.
Det er eksempler fra arkeologien at man har avdekket forholdsvis komplekse samfunn der
man likevel ikke har funnet spor av penger.
Man har dermed fastslått at det ikke fantes penger i disse samfunnene.
Det behøver slett ikke være riktig konklusjon.
Det kan like gjerne tenkes at pengene hadde en form som ikke la igjen fysiske spor
dersom menneskene i samfunnet ikke lever lenger.
Et slikt samfunn er beskrevet i boka til Øystein Kock Johansen, Fra stenpenger til Euro [#JohansenStenEuro]_.

.. Side 180

Forskjellige former for penger gjennom historien
------------------------------------------------

Penger som lett slites ut
~~~~~~~~~~~~~~~~~~~~~~~~~

I Mexico (Mayakulturen) brukte man ørsmå miniatyrøkser i kobber som var bare 0,14 mm tykke.
De mistet derfor sin verdi etterhvert som de ble slitt, og måtte tas ut av sirkulasjon og
smeltes om til nye penger. Det er lett å anta at pengene som etterfulgte
var overlegne disse skrøpelige øksepengene. De forbedrete pengenen
ble innført av Spanjolene som erobret området og de innførte mynter slik vi kjenner dem.
Disse myntene overtok etter hvert helt for øksepengene, men begge levde side om side
en god stund.
Hvorfor antar vi egentlig at varige mynter er overlegne skrøpelige tynne penger?
Ser man på penger som det det er, nemlig rasjonell informsjon, så kan man like
gjerne påstå at det krever utpreget evne til abstrakt tenkning for å enes om at
man kan "legemliggjøre" denne informasjonen i lett håndterbare metall-flak,
der det slett ikke er slik at alt som finnes av dette metallet automatisk *er* penger,
men at for at det skal bære informasjonen om penger må tilvirkes etter avtalte
retningslinjer, høyst sannsynlig også av penge-håndverkere som er godkjent av en
samfunnsautoritet eller etter et demokratisk prinsipp slik at man på en veldefinert
måte kan holde styr på pengemengden. Videre er sannsynligvis dette med slitasje
faktisk en veldig nyttig egenskap. Silvio Gesell beskrev dette i sitt verk
"The Natural Economic Order" [#Gesell]_.
Han sier at det er essensielt at penger mister sin verdi med tiden,
akkurat som alle andre varer.
Hvis ikke pengene mister sin verdi med tiden, er sannsynligheten for at
personene i samfunnet venter med å bruke dem større.
Da tenderer pengene til å slutte å sirkulere, og penger som ikke er i bruk
er stort sett samme tingen som at pengene ikke eksisterer.

.. Side 49 Fullkomne penger og primitive penger (Kock Johansen)

Tykkelsen på øksepengene var nok valgt med omhu. Ved at de ble slitt,
for alt vi vet kunne kanskje slitasjen være systematisert ved at man
*skulle* lage et merke, hakk i dem hver kan de var brukt og at de ble mindre
verd for hvert hakk (husk penger er informasjon!). Uansett, verdien på
hver øksepenge ble redusert enten gradvis eller plutselig, så de tilslutt
ikke var gyldige.
Dermed kan man få til verdireduksjon uten å utvanne pengemengden ved å
øke den.
Pengesystemer der man har en fast pengemengde, f. eks. at all forekomst
av et metall pr. definisjon *er* penger, har nettopp det iboende
problemet at varigheten av pengene gjør at sirkulasjonen av dem tenderer
til å opphøre.

.. Side 184 Johansen

Kulturer som tilsynelatende ikke hadde penger
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En annen interessant opplysning i boka til Johansen er om Inkariket i
Ecuador så sent som 1438 til 1532. Her han man ikke funnet noen spor
etter penger, til tross for et velutviklet stat og god infrastruktur.
Man antar derfor at samfunnet var bygd opp slik at penger ikke var
nødvendig.
Nå vet ikke jeg så mye om Inkaene, men jeg har spekulert om det
ikke like gjerne kan være slik at Inkaene visste om konseptet penger,
brukte penger, men at konseptet deres ikke inkorporerte noen
for fysisk form, at informasjonen kun var holdt i live av at utpekte
personer husket saldoene til de forskjellige samfunnsmedlemmene.
Dette er jo ikke så ulikt til hvordan vårt nåværende system for
å holde rede på saldoer fungerer.
Nå for tiden er det riktignok ikke i menneskers hjerner informasjonen ligger,
nei i vår tid har vi funnet opp roboter til slikt, de kalles gjerne servere
(tjenere) eller mainframes eller datamaskiner.
Samme prinsipp har jo blitt brukt andre steder i andre sammenhenger,
eksempelvis *lovsigemann* på Island.
De hadde som oppgave kunne lovene utenat og muntlig foredra alle landets
lover og rettsregler på Alltinget.

.. https://snl.no/lovsigemann

.. side 221, det 5. trinn

Kulturer der penger ikke "skiftet hender" i tradisjonell forstand
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Et annet bemerkelsesverdig pengesystem som er beskrevet til boka "Fra stenpenger til Euro"
er nettopp de stenpengene som tittelen antyder og som er avbildet på utsiden av boka.
Stenpengene var i bruk i Mikronesia-føderasjonen (Yap), og er i og for seg fremdeles i bruk
ved siden av US dollar.
Disse pengene er så store og tunge at de ikke ble flyttet etterhvert som de brukt
som gjenytelse for varer og tjenester.
De står utendørs slik at alle kan se dem, og når en handel var gjennomført var det
på bakgrunn av at det var allment kjent hvem som var den nye eieren at hver enkelts
saldo, for å bruke det uttrykket, kunne holdes rede på.

Det kan altså virke som at vi her har et bank-system som er implementert inne i
folks hjerner hva. minne-palass-teknologi [#MethodLoci]_. Alle stenpengene har faste plasser, står
utenfor eller rundt folks hus eller langs veier og stier, og alle har litt forskjellig
form, og de finnes i forskjellige størrelser. Da blir oppgaven med å knytte personers
eierskap til de forskjellige stenene ikke så uoverkommelig. Man har også implementert
en sikkerhetsmekanisme i systemet når det er mange hjerner som inneholder duplisert
informasjon.

Kassakreditt, LETS og andre private penger
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Kassakreditt er et spesialtilfelle av lån eller kreditt der man har fått innvilget en viss
ramme som man selv kan skape nye penger innenfor.
For hva skjer når man begynner å bruke kreditten? Hvis du har 0 kr. på konto og betaler
en regning på kr. 10000 til en annen,
så betyr jo det at det blir kr. 10000 mer i systemet, M øker med kr. 10000,
mens på din egen konto blir det negativ saldo, som gjør at K øker tilsvarene.
Når du senere får inn lønn på kontoen, f. eks. kr. 15000, så blir de pengene som du skapte
borte igjen, og både M og K reduseres tilbake til det de var.

Dette er forøvrig nokså nøyaktig samme måte som det mest populære alternative
private pengesystemet LETS virker [#LETS]_.
Summen av alle kontoer i et LETS-system er null,
og det er ikke noe problem at du har negativ saldo.
Hovedforskjellen i forhold til "ekte" banker er at i LETS beregnes det ikke renter,
så du taper ingenting på å ha permanent negativ saldo i LETS
(men du vinner ikke noe på å ha positiv saldo heller).

Eksempler på andre private pengesystem er bonus-poeng til flyselskaper,
og til og avtaler som folk gjør med hverandre at de skal betale senere.
For hvis du kjøper en sykkel av naboen, men ikke har pengene akkurat nå,
så kan det tenkes at dere blir enige om at du kan vente med å betale den
500-lappen.

For hvis det så finnes en annen nabo som har en snøfreser som
han som hadde sykkelen har lyst på, så kan de to bli enig om å kjøpe den med den 500-lappen
som han ikke har fått av deg ennå slik at du nå plutselig skylder naboen som hadde snøfreseren
500 kroner i stedet for han som du kjøpte sykkelen av.
Så selv uten pengene, kan han handle basert på avtaler mellom dere 3 naboer.

Da har dere i realiteten utvidet pengemengden (selvom dette ikke blir synlig i M).
Grunnen til at man kan si dette, er at den 500-lappen du senere
får tak i og betaler med, den har jo vært i bruk ett eller annet sted i mellomtiden
samtidig som du allerede har begynt å bruke sykkelen og han med sykkelen har fått seg
snøfreser.

Akkurat som med bank-penger så er altså private penger egentlig avtaler mellom mennesker.
Og siden hvemsomhelst kan skape penger, så virker det ikke så urimelig at bankene
bruker den samme metoden når de skaper ekte penger.

Penger med og uten serienummer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Banksystemet vi har nå opererer uten serienummere på pengene (unntatt sedler)
- Bitcoins er en penge-implementasjon ikke bare med serienummer, men med krypterte
  serienummere slik at banksystemet kan gjøres distribuert i forhold til maskinvare
- (Det offisielle banksystemet er jo på mange servere, men ikke distribuert i
  samme betydning, med distribuert her menes at man ikke trenger å ha en overordnet
  kontroll på hvor i systemet pengene er, at enhver maskin i nettverket kan benyttes)
- Stenpenger (Yap) kan sier å likne på bitcoins, unike mynter og distribuert
  maskinvare
- Man *kunne* innført serienummere i vårt offisielle banksystem
- Det ville gitt behov for større lagrings-kapasitet
- Hvis man da benytter fifo-prinsippet for konto-transaksjoner (first in - first out),
  så kunne man målt (satt opp statistikk) for omløpshastigheten for penger
  (Kvantitets-teori m*v)
- Bedre mulighet for å spore pengestrømmer (på godt og vondt)
- Ikke sikkert dette ville være av så stor praktisk betydning at man tar seg bryet

Lånemengden må øke med tiden
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dette må plasseres et annet sted i dokumentet [#99]_

Måten de pengene vi bruker idag mister verdien sin på er nettopp ved
utvanning vha. økning av pengemengden vha. utlån eller rettere sagt kreditt.

Hvis ikke nye generasjoner låner et høyere kronebeløp enn den generasjoen
som betaler tilbake lånene sine, så blir det problemer med å skaffe penger
til å betale tilbake avdragene, for uten nye lån, ingen nye penger til å betale tilbake
gamle lån med.

Dette er hovedårsaken til at pengesystemet er selvforsterkende i negativ retning
når ting først har begynt å gå dårlig.
Når den økonomiske aktiviteten bremser, så går prisene ned, behovet for å ta opp store
lån til f. eks. huskjøp avtar, følgelig avtar lysten til å ta opp nye lån.
Etablerte kunder som allerede har store lån sliter da med å få tak i de pengene som finnes,
for det blir jo færre penger i omløp når ingen er villige til å ta opp nye lån (som skaper penger)
mens folk som betaler inn avdragene sine sørger for at pengemengden skrumper inn.
Når folk i tillegg opplever at prisene hele tiden synker tenker de gjerne at de bør vente
med å handle fordi da får de mer igjen for pengene sine hvis de venter med å kjøpe,
og dette forsterker virkningen av nedgangstidene ytterligere.
Dermed sliter også bankene fordi mange lån nødvendigvis må bli misligholdt.

Min pengteori
-------------

Noen økonomer etterlyser av og til et enklere banksystem der bankene og deres
ansatte ikke faller i fristelse å gjøre altfor mange "smarte" ting som til
sjuende og sist kan destabilisere hele banksystemet.
I dokumentet "sitater.pdf" siterer jeg Arne Jon Isachsen der han kommer inn på
dette.
Det jeg skriver nedenfor er sterkt inspirert av Silvio Gesells tanker og ideer.
Han skrev sitt verk rundt 1910 til 1918.
Da var det gullstandarden som fortsatt gjaldt.
Gesells system var radikalt forskjellig fra gullstandarden i og med at han innså
at holdbarheten av gullmynter (som vi identifiserer som mangel på inflasjon) var
en svært uheldig egenskap.
Gesell ønsket å skape et system der sedler mistet verdi.
Han implementerte dette med et frimerkesystem der man måtte kjøpe seg til
en fortsatt holdbarhet av sedlene.
Gesell var visstnok klar over at inflasjon (utstedelse av stadig mer sedler)
ville ha samme virkning, men var personlig imot en slik løsning.
Charles Eisenstein [#EisensteinSacredEconomics]_ anvender Gesells prinsipper,
men implementerer dem ved negativ rente på bankinnskudd og fravær av sedler.
Trond Andresen [#TrondAndresenFB]_ anvender etter det jeg forstår samme prinsipper
som Charles Eisenstein.
Min tilnærming (nedenfor) er å anvende inflasjon. Alle tre metodene er i prinsippet
helt like (bortsett fra regnemessige detaljer), og har det til felles at man
innfører en skatt på bankinnskudd og sedler som tilfaller fellesskapet.
Jeg tillater meg å innføre noen tankemessige modeller som jeg tror vil være
til hjelp for å forstå hvordan det hele fungerer.

Vi kan tenke oss at vi vil bygge en bank der det er et materiale som det finnes virkelig
mye av som blir brukt til å representere penger.
Dette vil stå i kontrast til pengesystemer der all forekomst av ett eller annet,
for eksempel gull, ble betraktet som penger.
Det er jo, når vi tenker etter, utallige ganger i historien løp hvor kun en avtalt mengde
av noe har vært brukt som penger.
Øksepengene til Mayaene er ett eksempel. Skjell har også
vært i bruk, men da ikke lokale skjell hvem som helst kunne finne, men importerte skjell.

Nå idag, etter at gullstandarden er forlatt (i 1931) ser man på sedler og mynt som
mer reelle penger en konto-penger. Sedler og mynt i omløp utgjør bl. a. størstedelen
av det man kaller basispengemengden, som så kontopenger bygger på via utlån.

Her kommer også sentralbankenes såkallte seigniorage inn [#GlobalePenger]_.
Dette er bankvesenets inntjening på at man kan utstede sedler og mynt som har høyere
pålydende verdi enn det det kostet å produsere dem.
Samme begrep, seigniorage, brukes i litt forskjellige sammenhenger enn dette, nemlig at
ved å øke seddelmengden og dermed utvanne pengemengden kan dette ses på som en skatt på
penger.
Dessuten brukes det ved internasjonale transaksjoner fordi ett land vil ha en fordel av
at et annet land kjøper det første landets sedler siden det ikke er renter på sedler.

Disse resonnementene baserer seg på tildels en praksis som vi er i ferd med å forlate,
i og med at sedler og mynt etterhvert får en mindre sentral posisjon siden man går
mot større bruk av elektroniske betalings-metoder.
Et annet poeng er at man for at inntjenings-resonnementet skal holde
så må det alltid være en "motstrøm" av rentebærende verdipapirer når sedler utveksles.
Det er i og for seg tilfelle i "moderne" bankvesen,
men det paradoksale blir da: hvor kommer denne inntjeningen fra? Hvis sedler er de
de egentlige pengene, hvorfor kan verdipapirer, obligasjoner, som i utgangspunktet bare
er en slags lånekontrakt, telle såpass mye mer enn pengesedler at obligasjonene etter
en tid kan tiltrekke seg mer sedler enn pålydende i utgangspunktet?
Det hele er jo bare snakk om vedtatte spilleregler,
og i dette spillet er det vanskelig å få tak på hva som er utgangspunktet for spillet,
pengesedlene eller obligasjonene.
En merkverdig konsekvens av dette er at pengevesenet vil se på det som en inntektskilde
å utstede samlerobjekter i form av sedler og mynt med spesielle motiv og valører,
selvom disse objektene er laget med en pålydende verdi og er godkjent som betalingsmiddel.

Idéen her er at:

- Samlerobjektene er billigere å produsere enn pålydende
- Samlerene vil holde objektene utenfor vanlig sirkulasjon(!)
- Ojektene blir betalt med penger (konto-penger) som kan investeres av banksystemet i
  rentebærende papirer

På samme måte som vanlige sedler og mynt, ser man på dette som at publikum yter bankvesenet
et rentefritt lån ved at publikum bærer på sedler og mynt.

I dette bildet blir hele pengesystemet et sammensurium av verdipapirer og transaksjoner og
sedler som tilslutt tilslører hvilken aktør som har hvilken oppgave og hvilken nytteverdi
hver aktør har av de forskjellige transaksjonene og ikke minst hvem som "eier" pengene.

Jeg vil derfor tilbake til utgangspunktet, penger er kun rasjonell informasjon som kan bli
representert på de forskjelligste måter.

Reell (virtuell) implementasjon av penger
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I dette tankeeksperimentet skal vi bruke ørkensand.
Men naturligvis ikke all ørkensand.
Kun den sanden som på forsvarlig vis har har gått gjennom inngangskontrollen i banken.
Banken må derfor være en fullstendig tett bygning, slik at ikke sand kan blåse ut eller inn.
Inngangkontrollen kan være så enkel som å hente sand, veie sand og levere sand inn i banken,
eller den kan bestå i å studere hvert sandkorn inngående og forkaste sandkorn som ikke lever
opp til en viss standard (form, størrelse, vekt) og til og med muligens må hvert sandkorn
inngraveres med et serienummer som man senere kan lese av i et mikroskop.
Det er et poeng at det skal være nær sagt uendelige mengder med sand som *kan* bli redefinert
til penger dersom behovet skulle melde seg slik at ikke pengesystemet kan komme inn i
problemstillingen at det er ikke kan ekspandere mer.

Inngangskontrollen har i oppgave å levere sand inn i banken etter hvert som en ytre mekanisme
dikterer at det skal genereres nye penger.
Denne ytre mekanismen kan f. eks. være at en kreditt-institusjon sender melding om at et nytt
lån er invilget, og hvilke konto disse nye pengene skal godskrives.

Når sanden flyttes fra inngangskontrollen og inn i banken, så er det fordi det finnes en eier
av denne sanden. Bankens oppgave er å oppbevare sanden for kunden, men den betraktes som kundens
eiendom, ikke bankens så lenge den er inne i banken.
Sanden som ligger i inngangskontrollen er ingen sin eiendom, eller, det er banken som disponerer den,
men i inngangskontrollen har ikke sanden større verdi enn sand har,
den regnes ikke med i pengemengden, og kunne således like gjerne ligget ut i ørkenen blandt alle
de andre sandkornene.

Hver kontohaver i banken har sitt eller sine glasskolber som sanden (pengene) blir oppbevart i.
Forbindelsen mellom banken og kundene er kun via telefon eller internet eller en annen form
for avstands-kommunikasjon. Romerene ville naturligvis brukt semaforering fra fjelltopp til
fjelltopp.
Når man er i butikken og handler og setter bankkortet inn i terminalen, går det en melding
til banken om hvor mye sand som skal flyttes ut av min glasskolbe og over på riktig konto
som tilhører butikken.

Som sagt skal sand flyttes inn i banken bare dersom en kunde har fått invilget lån siden
det er dette som er mekanismen bak å skape nye penger.
Når avdrag blir betalt tilbake skal en mengde sand tilsvarende dette kastes ut av banken
igjen i utgangskontrollen.
Her også må det være sikkerhets-systemer i banken som gjør at man kan vite at hvor mye
sand som kastes ut tilsvarer det som er betalt tilbake i avdrag.
Dersom man har brukt mye resurser i inngangskontrollen, f. eks. gravert inn serienummere
i hvert sandkorn, vil det være naturlig å ha et ventelager mellom utgangskontrollen og
inngangskontrollen.
Sandkorn som ligger i dette ventelageret skal ikke regnes med i pengemengden.
Det betyr at inngangskontrollen kan bruke ferdig-brukt sand istedet for at all sand som
skal inn i banken blir tatt utenfra.
Som regel i pengesystemer er det jo slik at pengemengden sjelden går ned, så i praksis
trenger vi derfor aldri å kaste ut sand fra ventelageret og ut i ørkenen igjen.

Ett unntak inntreffer når det har blitt så mye sand i banken at det ikke er plass til mer.
Da må man reformere systemet ved at alle innskudd og alle lånepapirer blir justert ned
med en faktor 100 eller 1000 eller hva man blir enige om.
Hvis faktoren er 100 må man fjerne 99% av all sand.
Alle samfunnsborgere må venne seg til de nye prisene som blir en hundredel av hva de var
dagen før. For å markere overgangen gir man den nye vautaen et lite avvik i navnet, f. eks.
Nykroner, slik at 1 Nykrone er lik 100 Kroner. Hvis man lager ventelageret stort nok, kan
man naturligvis legge alle de 99% overflødig sand der og dermed antakelig aldri trenge å
hente helt ny sand utenifra igjen.
Sanden på ventelageret skal jo ikke regnes med i pengemengden uansett.

Som vi ser kan sand godt representere pengene dersom man er skeptisk til at de bare skal
være representert i en eller flere datamaskiner. Det er likevel ikke store forskjellen på
sand eller tall i en datamaskin.
Uansett representerer de jo det samme, nemlig informasjonen om fordelingen av kjøpekraft.
Å blande inn et reelt stoff som har en egenverdi i seg selv, som for eksempel gull,
er helt klart et blindspor.

Det er naturligvis ikke noe poeng og faktisk bygge en slik bank.
Det kan likevel være et nyttig redskap å tenke seg en slik bank for å holde orden på begrepene
og for bedre å forstå hva penger engentlig er. Man kan jo f. eks. lage en simulator av en slik
bank slik at man kan "se" hvordan den fungerer.
Dette kan nok ganske enkelt realiseres som en 3D-modell på en generell data-spill-platform.

I den virkelige verden er jo bankene organisert slik at de har mange forskjellige oppgaver
eller ansvarsområder, og de er nå som regel organisert som aksjeselskaper med eiere som
kan tenkes å investere i bankene for å oppnå et utbytte.

Tatt i betrakning sand-modellen er jo det å oppevare sand i glasskolber for folk en forholdsvis
kjedelig oppgave. I vår virkelighet er det i og for seg hver enkelt bank som sitter med
disse glasskolbene, men de kan også finne på å midlertidig overføre mer eller mindre av innholdet
til sentralbanken eller låne det bort til andre banker.
Når det gjelder utlånsdelen av de reelle bankene vi opererer med så sitter hver enkelt bank på lånepapirene til sine kunder,
men også disse kan skyfles fram og tilbake mellom banker for å utlikne ulikheter og for å få
høyest mulig uttelling på investeringer, eller de kan være grunnlaget for å generere andre
verdipapirer som kjøpes og selges mellom banker eller handles med av andre aktører.

Her kommer en skisse til
en enklere bank-struktur enn den vi har idag der målet skal være:

- Banken skal være en tjeneste for folk uten at det er et poeng at eiere skal kunne ta ut
  utbytte dersom banksystemet like gjerne kan fungere uten risikovillige eiere
- Sikre seg mot at mislighold av lån kan velte hele banksystemet over ende

Skisse over bankenes funksjon og virkemåte i et lukket system med kun én valuta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Som vist over kan man tenke seg pengesystemet implementert vha. ett eller annet materiale som det
finnes nær sagt uendelige mengder av, og så definere en metode der en avgrenset andel av dette
materialet er definert som penger. Her følger en skisse over et bank-system, men for å forenkle
tankegangen så ser jeg her bort fra at det finnes noe som heter "utlandet" som har sine egne
valutaer.
Med en gang man tar med andre valutaer og at penger kan flyttes fram og tilbake mellom
valutaområder blir en tilsvarende skisse mer komplisert.
Det finnes gode forslag for hvordan valutaområder skal inter-agere også.
John Maynard Keynes og  E. F. Schumacher foreslo et glimrende system som ble lagt fram
på Bretton Woods-konferansen i 1944, men der dessverre Amerikanske delegater vant drakampen
hvilket de facto gjorde Dollaren til en slags verdensvaluta. Mer om dette senere.

Det kan uansett være nyttig å analysere en forenklet modell fordi man trenger å danne seg
mentale bilder av hva som skjer innenfor slike systemer, og da må man starte med forenklinger.

Jeg tenker meg også at kvantitativ lette er i bruk, og at denne fungerer som skissert i
kapittelet om `Pengetrykking`_,
altså på bakgrunn av en ny type samfunnskontrakt som ikke automatisk lager en motpost i
form av gjeld slik at vi har M > K.

Jeg ser for meg at bank og finans bør være helt adskilt.
Med det mener jeg at omsetning av verdipapirer som aksjer og obligasjoner på vegne av kunder
ikke foretas av banker,
men av egne institusjoner som kan være i privat eie i en eller annen form.
Dette var også et viktig moment i 1933 etter finanskrisen da Glass–Steagall Act ble avtalt
for å forhindre sammenblanding av roller innen bankvesenet.
Denne avtalen er ikke lenger gyldig, den ble trukket tilbake i 1999.
I dette kapittelet er det kun bankvesenet som blir omtalt.
Dette utelukker riktignok ikke at bankene eier verdipapirer i form av aksjer eller obligasjoner
som en del av sin egenkapital dersom man finner at dette er formålstjenlig.

Innskudd
^^^^^^^^

Det å holde greie på mengden penger på konto til hvert enkelt samfunnsmedlem er en triviell ting
som man like gjerne kunne overlatt fullstendig til sentralbanken. I praksis når man logger seg inn
i nettbanken kunne man altså sett rett inn i et system som blir vedlikeholdt av sentralbanken.
Trond Andresen [#TrondAndresenFB]_ tenker seg også et slikt sentralisert system,
og dette er også beskrevet i 'A better monetary system for Iceland' [#IslandskPengeReform]_.
Likevel kunne slike praktiske ting som utsteding av bank-kort o.l. gjøres av private aktører.
Nettbanken kunne i så fall også gjerne vært en portal hos den private aktøren men som likevel er
direkte koblet opp mot sentralbankens database der konto-opplysningene ligger.

Alle pengene på slike kontoer *er der* og er eid av kontohaveren.
De kan ikke forvaltes og brukes av sentralbanken til å investere i egne eller andres verdipapier.
Konto-opplysningene kan altså sees på som et
speilbilde av det som er satt inn i sand-banken i ørkenen
(som vi ble enige om at vi ikke trenger å bygge).
Kontoene i sentralbanken *er* rett og slett sandbanken.
Materialet som det finnes uendelig mye av hvor en andel av dette er satt under kontrollerte
former og derfor er definert som penger,
er altså ikke sand, men tall i sentralbankens mainframe.
Så lenge disse tallene behandles på samme måte som om det var sanden i ørkenbanken, at det ikke
plutselig oppstår eller forsvinner tall utenom de mekanismene som er definert til det,
så spiller det ingen rolle som det er fysisk sand, eller et annet materiale eller bare tall i
en mainframe.

Det at pengene *er der* betyr jo ikke at de ikke kan investeres.
Hvem som helst kan bestemme seg for å kjøpe verdipapirer for sine egne penger.
Men dette fører jo ikke til at pengene forsvinner,
de blir jo bare flyttet fra kontoen til den som kjøper til den som selger verdipapiret.
Hvis verdipapiret må selges igjen for en lavere pris enn det ble kjøpt for, så vil jo heller ingen
penger bli borte av den grunn. Den som selger verdipapiret med tap har jo tapt penger,
med disse pengene vil jo da bli gevist for én eller flere andre kontohavere.

Innskuddsrente
^^^^^^^^^^^^^^

Innskuddsrente vil man høyst sannsynlig ønske å ha.
Det vil riktignok ikke være et stort poeng å tilby forskjellig rente avhengig av konto-type og saldo.
Hovedpoenget med innskuddsrente slik jeg ser det er at folk skal foretrekke å ha pengene i banken
i forhold til å ha liggende kontanter.
Jeg mener faktisk at å tilby mer rente jo høyere saldo du har virker motsatt av hva som er å foretrekke.
Dersom du har mye penger, så burde du klare deg helt fint uten å ha høy rente som en fordel.
Det er jo de som ikke har mye penger på konto som trenger penger mest!
Det er jo heller ikke slik at penger forsvinner direkte ut av banksystemet
(når man inkluderer kontanter) avhengig av rentenivået på innskudd.
Når aktører handler med hverandre flyttes jo bare pengene mellom kontoer, pengemengden er den samme.
En flat rente som er så liten som mulig, men ikke *så* liten at folk heller har pengene siden oppbevart
hjemme som kontanter bør være optimalt. Det at banker tiltrekker seg kunder ved å konkurrere ved å
ha høyest mulig rente faller bort dersom det er sentralbanken som vedlikeholder hele systemet.
Man trenger heller ikke ha noen direkte kobling mellom innskuddsrente og kreditt-rente.
Hele innskuddsrenten kan være en ordning som sentralbanken står for der kilden til denne renten er
en andel av den generelle kvantitative letten. Kvantitativ lette kan jo ses på som skatt på penger,
og husk, dette gjelder også kontanter, siden det å tynne ut pengemengen også går utover kjøpekraften
til kontanter.
Når man lar en andel av den kvantitative letten bli brukt som innskuddsrente,
så er det i praksis det samme som å differensiere skatten på forskjellige typer penger, det blir
mer skatt på kontanter enn på bank-innskudd.

Kontanter
^^^^^^^^^

Kontanter som man tar ut av banken må behandles som et særtilfelle.
En logisk måte å behandle dette på er at det finnes en sentralbank-konto som er lik summen av
alle kontanter som er ute. Hver gang man tar ut kontanter flyttes det "sand" fra din konto
til denne kontant-speil-kontoen som det altså bare finne én av.
Når noen setter inn kontanter i banken flyttes "sand" fra kontant-speil-kontoen
til kundens konto.
Kontanter som er inne i banken, i minibanken f. eks. regnes ikke med i pengemengden.
Kontantene blir penger som regnes med i pengemengden først når en kunde besitter dem.
Man kan derfor like gjerne regne kontant-speil-kontoen til pengemengden siden denne tilsvarer hvor mye
kontanter som er i omløp.
Pengemengden endrer seg dermed ikke idét man tar ut kontanter fra en minibank.
Det er bare sand som flyttes fra kundens konto til kontant-speil-kontoen.
Pga. at kontanter forsvinner, mistes eller ødelegges f. eks., så må sentralbanken estimere dette og gjøre en kompenserende
transaksjon en gang iblant slik at ikke speilkontoen vokser seg mye større enn det som reelt sett
er ute blant publikum.
I forbindelse med at pengesedler eller mynt går ut av bruk og erstattes med nye får sentralbanken en definitiv
måling på hvor stor del av kontantene som har forsvunnet.

I kontant-speil-kontoen vil det antakelig være fornuftig å ha flere opplysninger enn kun samlet sum av kontanter ute.
man kan tenke seg at antall av hver valør i omløp blir registrert, og til og med hvilke serienummere av hver seddeltype.

Utlån
^^^^^

For utlån (kreditt) er saken litt anderledes.
Det kan synes å være uhensiktsmessig å la sentralbanken stå for dette.
Her trengs det et system som vurderer låne-søknader på forskjellige
prosjekter, det være seg hus-kjøp eller investeringer i maskin-parken i en bedrift.
Dette systemet trenger høyst sannsynlig mennesker som tar avgjørelser.
La oss tenke oss at det er rimelig at det er ikke-offentlige instutisjoner som tar seg av dette.
La oss kalle disse for kreditt-banker og at kreditt-bankene ikke har bankinnskudd som ett av sine
arbeidsfelt.
Det eneste kreditt-bankene da har som aktiva er låne-kontrakter.

La oss undersøke om kreditt-bankene trenger reserver for å "rydde opp i" situasjoner der lån har blitt misligholdt.
Først en enkel men urealistisk antakelse om at ingen misligholder sine lån. Hva trengs for at en ny-etablert
kreditt-bank kan starte opp?
Det den trenger er noen mennesker og en database eller arkiv for å holde orden på kundene.
Dette utløser åpenbart nødvendigheten av litt startkapital.
La oss si at dette er ordnet og at kreditt-banken mottar sin første kunde.
Foreløpig finnes det ingen reserver i banken,
men banken har noen kontoer i sentralbanken der slike ting som lønnsutbetalinger, rente-inntekter o.l. blir ført.
En annen konto blir brukt til å overføre kundenes avdrag til.
Kreditt-banken har altså en driftskonto og en kunde-avdrags-konto i sentralbanken.

Den første kunden kommer og får innvilget kreditt.
Kreditt-banken mottar kontrakten der skyldig beløp og rente- og avdrags-betingelser er nedtegnet,
samt evt. pante-papirer.
Kreditt-banken setter opp en kundekonto med negativ saldo og sender beskjed til sentralbanken om at den må
opprette en konto med positiv saldo for denne kunden der det er penger som sentralbanken skaper som settes inn.
Sentralbanken henter altså mer sand fra inngangskontrollen i ørken-bank-analogien.
I denne prosessen øker da M og K med det beløpet som er lånt ut, akkurat som det skal.
Kreditt-banken oppretter også en rentekonto for kunden der den løpende renten blir beregnet.

Kreditt-banken tjener penger ved at rentene som kunden etterhvert betaler går inn på driftskontoen og ut igjen
til lønnsutbetalinger og leie av lokaler og database-tjenester osv., samt at innbetalt rente inngår i beregningen
av kundens rentekonto.
Når kunden betaler avdrag overfører kreditt-banken dette til avdrags-kontoen samt at kundekontoen blir godskrevet
med samme beløp, og dermed altså mindre negativ.
Sentralbanken må ha en mekanisme som behandler kreditt-bankenes avdragskontoer som et spesialtilfelle, fordi dette er et signal
om at en kundekonto i en kreditt-bank har blitt godskrevet. Det sentralbanken gjør er å tømme denne kontoen
(ut av banken, altså ut gjennom utgangskontrollen til ventelageret i sand-banken) og sette innstående til null igjen.
Dermed sikrer man seg at denne prosessen fører til at M og K synker med beløpet som tilsvarer avdraget.

Så lenge ingen misligholder lånene sine, trenger åpenbart ikke kreditt-banken å sitte på noen reserver, eller
å sjonglere med kundenes penger på innskuddskonto for å balansere ut hvor mye som er lånt ut.

Utlånsrente
^^^^^^^^^^^

Beskrevet i [#99]_. Prinsippet er at man må ha mekanismer som sørger for at kreditt-bankene låner ut uten hemninger,
fordi at dette i seg selv vil skape hyperinflasjon. En mulig mekanisme er rente-størrelsen.
Dersom kreditt-banken er organisert som et andels-lag av noe slag som ikke er interessert primært i profitt,
men å kunne tilby en nyttig tjeneste, kan man tenke seg at en overvåkende offentlig enhet regulerer renten
ved å kreve inn en skatt fra kreditt-bankene. Disse må da kreve høyere rente enn dette fra kundene, slik at
den differansen de sitter igjen med er tilstrekkelig for å sørge for driften av kreditt-banken.
Det bør ikke være noe poeng i seg selv at det offentlige melker skatt ut av dette systemet,
så i normal-tilfellet kan denne skatten være null, altså så lenge den totale kreditten, K, ikke overskrider
maksimums-målet.

Mislighold av lån
^^^^^^^^^^^^^^^^^

Behandling av lån som blir misligholdt vi bringe inn en faktor som vil kunne forutsette at banken har egenkapital-reserve
som den kan rydde opp med.
Men er det sikkert at banken trenger egenkapital for dette? Hva slags egenkapital?
Det som er nokså sikkert (men ikke åpenbart 100% sikkert) er at når noen misligholder lånet sitt så er det noen
andre som må steppe inn å gjøre opp istedet. Hvorfor ikke 100% sikkert? Jo, sentralbanken kan jo bare i teorien trykke på
knappen for å skape like mye penger som ikke ble betalt inn å betale med dette beløpet.
Men hva skjer dersom dette er måten å løse det på, jo, vi får en utvanning av pengemengden som altså overfører litt av
kraften fra alle eksisterende penger til de ny-skapte pengene.
Altså kan vi vært praktisk talt 100% sikre på at noen andre må gjøre opp det som ble misligholdt, men disse "noen andre"
kan tenkes å være absolutt alle som har penger.

Man trenger naturligvis også mekanismer for å begrense at man kommer i en situasjon med mislighold av lån.
Det kan tenkes å være en straffereaksjon mot den kreditt-banken som gjentatte ganger oppretter kontrakter som fører til
mislighold, og/eller en straffereaksjon mot kunden som ikke kan betale tilbake lånet.

Hvilke grupper av mennesker eller hvilke enkelt-mennesker kan vi se for oss gjør opp for de misligholdte
lånene? Aktuelle grupper kan være: De som eier den kreditt-banken det gjelder, de som har lån i den kreditt-banken det gjelder,
alle som har lån i en eller annen kreditt-bank, og alle som har innskudd.

For endel av disse variantene kan man resonnere seg fram til at effekten når man betrakter sekundær-virkninger
er at alle samfunnsmedlemmer blir involvert uansett. Ta f. eks. eierene av kreditt-banken.
Disse må jo hente pengene fra et sted og avhengig av hva slags eiere det dreier seg om så kan de ha
virksomheter innenfor andre bransjer enn kreditt-bank. Hvis de bare opererer innenfor kreditt-bank må de jo
sørge for å overføre midler fra kreditt-banker med overskudd til kreditt-banker med tap, og dermed vannes virkningen
ut som en sekundær-effekt.
Dersom de er i andre bransjer, f. eks. mat, vil jo samtlige forbrukere av mat bli trukket inn.
Når det gjelder å prøve å begrense virkningene til bare låne-kunder så vil jo dette via renter o.l. også kunne
vannes ut så alle i praksis blir involvert.

Egenkapital-reserve
^^^^^^^^^^^^^^^^^^^

Som vist over kan det tenkes at egenkapital er overflødig selv i et bank-system der man tar høyde for mislighold av lån.
Det vi trenger er en mekanisme som sørger for at alle som har penger direkte bidrar til å rette opp misforholdet
som oppstår fra mislighold. En fordel ved dette er at de som har mest penger bidrar mest (generell uttynning av pengemengden).

Man må jo også tenke på hva som er konsekvensene av at man krever at kreditt-banken skal ha en reserve.
La oss si at reserven skal være penger på en egenkapital-konto i sentralbanken som er eid av kreditt-banken.
Kreditt-banken må hente disse pengene fra eierene eller lånetakerene (sette av endel av rente-inntekten) eller en kombinasjon.
Beregning av pengemengden gjøres normalt på den måten at man ikke regner med penger i finansiell sektor, altså penger
eid av bankene.
Dette er en riktig vurdering siden det som betyr noe i samspillet mellom real-økonomien og pengene er hvor mye penger som er i omløp.
Penger på en egenkapital-konto som er eid av bankene er definitivt ikke i omløp, de skal ideelt sett bare stå der og aldri komme i bruk.
Hvis de må brukes så er dette bare en nødventil for å oppveie en uønsket effekt.
For at kreditt-banken skal ha egenkapital så må den altså "inndra" en andel av pengemengden.
Dette betyr at pengene som kundene må få tak i for å kunne betale tilbake lånene skrumper inn i forhold til hvor mye lån som eksisterer.
Systemet med egenkapital i bankene øker altså sannsynligheten for at man skal komme i en situasjon der lån blir misligholdt.
Som motvekt mot dette kan myndighentene kreve enda høyere egenkapital, men dette vil jo ytterligere øke sannsynligheten
for mislighold av lån, og da har vi det gående...

Det kan synes som en bedre idé å vanne ut effekten av mislighold selvom dette isolert sett kan virke som en fullstendig
urettferdig ordning.
Poenget er at hvis man ser samfunnet fra et makro-nivå, så er vi til sjuende og sist i samme båt og må innrette oss deretter
i stedet for å late som om vi *ikke* er i samme båt.
Det virker jo minst like urettferdig det som skjer i praksis, nemlig at statene må redde bankene og at dette gjøres med
skattebetalerenes penger.

Hva så hvis egenkapitalen til kreditt-bankene ikke er konto-penger men verdipapirer, aksjer og obligasjoner?
Da vil jo ikke egenkapital-kravet føre til inndragning av pengemengden.
Det stemmer jo, men det blir litt som å dra seg selv opp med sine egne bukseseler:
Hvis egenkapialen skal brukes, må jo banken selge noen av verdipapirene, og disse står i større fare for å miste sin
verdi i parallell med at vi får store mengder av misligholdte lån, for hvis pengesystemet har tendenser til å kollapse, så finnes
det færre penger der ute slik at omsetningen av verdipapirer lider.

.. _Pengetrykking: internal-crossref

Pengetrykking, kvantitativ lettelse og "One man's dept is another man's money"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I moderne pengesystemer har vi som nevnt to makro-størrelser som er summen
av alle innskudd og sedler og mynt (M) og summen av all gjeld og kreditt (K).
(Se "Pengemengde_ og Kredittindikator_").
Disse skulle vært like store, fordi penger og gjeld er to sider av samme sak;
for at penger skal kunne oppstå må det samtidig oppstå like mye gjeld.
Bank- og kreditt-vesenet tjener på differansen mellom utlåns-renten
og innskuddsrenten.
Det betyr at de må bruke noe av utlånsrenten til å betale ut innskuddsrente,
mens de kan bruke resten til lønninger, leieutgifter oppbygging av egenkapital
og avkastning til eierene.

Ett problem her, som hverken jeg eller, tror jeg, Thomas Piketty helt forstår,
er at K er større enn M.
(I Pikettys tekst gjelder dette landenes utestående overfor utlandet sammenliknet
med landenes gjeld overfor utlandet, men i prinsippet er dette samme problem,
det finnes mer gjeld enn penger. Se nederst på side 560 i den norske utgaven av
[#Piketty]_.)
Dette finnes det muligens en naturlig forklaring på, men jeg lar dette ligge
ubesvart her, og setter opp som en antakelse at M = K.

Pengetrykking, kvantitativ lettelse, gjøres etter de samme prinsippene;
for å trykke penger må sentralbanken ha et gyldig dokument som er
den motsvarende gjeldsposten.
Dette kan for eksempel gjøres ved at sentralbanken kjøper opp gjelds-papirer
fra banker, eller ved at de kjøper stats-obligasjoner.
Disse verdipapir-typene er alltid tidsbestemt og de vil ha en rentegodtgjørelse
knyttet til seg. For å opprettholde et system med et tilsynelatende permanent
innslag av kvantitativ lettelse må man altså til enhver tid sørge for at
sentralbanken kan kjøpe tilsvarende nye verdipapirer når de som allerede
er kjøpt løper ut. Nettopp av denne grunn ses gjerne kvantitativ lettelse på
som et midlertidig tiltak som gjøres for å stimulere økonomien til å
"klare seg på egen hånd" etter at den har kommet over en "kneik".

Slik jeg ser det, er det fullt av misfortåelser og feil i dette resonnementet.
Dvs: så lenge man har som vedtatt norm at real-økonomien alltid, til evig tid
vil vokse, så holder resonnementet helt fint. "Kneiken" er da at økonomien
pga. av et uforutsett "sjokk" faktisk ikke vokser for tiden, så da gjelder det bare
å bruke det finansielle instrumentet som kvantitativ lette er
(sammen med andre vekstfremmende tiltak), til å sparke økonomien igang igjen.
Når alt så går i sin vanlige tralt med jevn økonomisk vekst, så kan tiltaket
med kvantitativ lettelse reverseres igjen ved at de oppkjøpte papirene løper ut.

Dilemmaet er at noen mennesker her i verden, og jeg er én av oss,
ikke tror på at evig økonomisk vekst er mulig.
Uansett hvor mye vi vil, så kommer vi ikke til å få det til.

Det vi derimot i teorien **kan** få til, er å lage et banksystem som ikke bryter
sammen selvom vi fra nå til evig til **ikke** har økonomisk vekst, ja til og med
dersom vi har svak negativ vekst.

Vi trenger en ny makroøkonomisk variabel, og en ny type verdipapir som
sentralbanken kan kjøpe av staten. La oss kalle dette nye prinsippet for
*Fiatpengepapir*. La oss kalle variabelen for F. La oss si at papiret er rentefritt
og at det ikke har noen definert utløpstid.
Dette er i tråd med begrepene Adair Turner bruker [#DebtDevil]_.
Turner sier riktignok ikke eksplisitt at det finnes et mot-papir i forhold til
de fiat-pengene som blir generert.

Staten kan altså velge og aldri trekke det tilbake, eller trekke det tilbake
når som helst. Når staten selger et slikt papir til Sentralbanken må Sentralbanken
trykke opp nye penger som ikke fantes fra før (dvs. sette nyskapte penger inn på
statens konto i Sentralbanken,altså genererer nye penger fiat).
Når staten trekker tilbake Fiat-penge-papiret må staten betale
pålydende til Sentralbanken som så må nulle disse pengene.

Staten vil kunne bruke disse pengene som inntekt på statsfinansene.
Det som skjer når disse pengene oppstår på statens konto, er ikke at staten
har blitt rikere (slik som Piketty forklarer i "Pengetrykking og nasjonal kapital"
på side 663 i [#Piketty]_).
Staten har omfordelt kjøpekraften i sin favør på bekostning av alle som har
(eier) penger. Mao. samme effekt som en flat skatt på penger. Samtidig betyr
dette en svekkelse av egen valuta i forhold til andre valutaer, forutsatt at ikke
andre valutaområder bruker den samme mekanismen. Dessuten vil dette på lang sikt
kunne føre til en pris- og lønns-vekst, altså inflasjon.
Til sammenlikning så svarer
jo også de private bankene sin utlånsmekanisme til at kjøpekraften blir omfordelt
fra de som eier penger til de som tar opp lån. Valutaen blir tilsvarende også svekket
gjennom denne mekanismen, og styrket av at lån blir betalt tilbake slik at
de private banken nuller dem igjen. Det samme gjelder tendensen til inflasjon/deflasjon.

Jo mer staten gjør av slike operasjoner (selger til Sentralbanken)
jo større blir F, altså mengden Fiat-penger.

Nå vil vi ha en ny makroøkonomisk monetær likning: F + K = M
Altså at summen av Fiat-penger og kreditt (gjeld) er lik pengemengden.

Hva er så Fiatpenger? Man kan se på det som et evigvarende rentefritt låneopptak
som staten gjør på vegne av samfunnet. De pengene som blir skapt vil jo bli
brukt til å dekke noen av statens utgifter til f. eks. folkeskole eller
liknende (og dermed vedlikehold av skolebygninger og lærerlønninger)
og vil dermed ende opp i den vanlige sirkulasjonen i pengesystemet.

Fiatpenger bryter altså med prinsippet om at det må være et realgode "bak"
enhver form for penger.
De fleste mainstream-økonom vil ikke anerkjenne
av et slikt forslag, for alle "vet jo" at penger må
knyttes til en verdi, og denne mekanismen med fiat-penger
peker ikke på noe objekt som
er noen sikkerhet, og dermed vil ingen ha tillit til slike penger,
vil han si, og dermed vil ikke pengene "virke".

Til det vil jeg si at sikkerheten ligger i menneskene i samfunnet.
Hvert menneske i samfunnet er en potensiell forbruker, og derfor
er penger nyttige også dersom ingen synes å ha bruk for å låne penger.
Så det er like intuitivt riktig å si:
"Så lenge det finnes mennesker har vi behov for penger", som å si
"Så lenge det finnes varer og tjenester, har vi behov for penger".

Ut fra likningen K = M (gitt F = 0) så ser vi at dersom alle
betalte tilbake lånene sine (K = 0), så forsvinner pengene (M = 0).
*Det* synes jeg ikke er særlig lurt.
Da er det hakket smartere med F + K = M, dvs. dersom alle betalte
tilbake lånene sine, så blir den gjenværende pengemengden M = F, altså
det som tilsvarer det rentefrie evigvarende lånet som staten har tatt opp
på vegne av befolkningen.

Det må være et regelverk for hvor mye slike verdipapirer staten kan generere.
Hvis staten genererer for mye, vil dette føre til hyperinflasjon.
Hvis staten genererer for lite og det er økonomiske nedgangstider, kan dette
føre til en uønsket innskrumpning av pengemengden, som i sin tur fører til
deflasjon, som alltid er mye værre enn inflasjon.

I en økonomi i vekst er det ikke så stort behov for antipenger. Der vil publikums
iver etter å ta opp lån være tilstrekkelig til å sikre at vi har en pengmengde
som vokser. Dersom vi går inn i faser med nullvekst eller negativ vekst, er
systemet med fiat-penger helt nødvendig for at vi kan sikre oss mot at banksystemet
kollapser. Dersom man etter en viss tid med nullvekst eller negativ vekst igjen
går inn i en fase i vekst,
er det ingen grunn til å trekke tilbake fiatpenge-papirene igjen.
Det man da må sørge for, er å heller å forhidre at K vokser over alle grenser, enten
ved generelt å stramme inn muligheten for kreditt eller ved rente-heving.

Merk dog dette: Fiatpenger er ikke det eneste som skal til for at samfunnet som
helhet skal kunne komme gjennom, eller til og med være i en konstant tilstand av
nullvekst eller svak negativ vekst. Problemet med negativ vekst er langt mer komplekst
og alvorlig enn at det kan enkelt fikses med et nytt finansielt instrument.
Det jeg derimot hevder, er at *uten* dette nye finasielle instrumentet,
så  er det helt utenkelig at man kan komme seg velberget igjennom en fase med selv
svak negativ vekst.

Adair Turner er ikke noen talsmann for nullvekst.
Han begrunner egentlig ikke *hvorfor* vi trenger økonomisk vekst,
men konkluderer med at en vei ut av uføret med stadig høyere
gjeldsgrad som vi ser skjer i vår økonomi,
så kan den tabu-belagte mekanismen som fiat-penger representerer
være løsningen. Jeg mener dette prinsippet er nødvendig uansett,
uavhengig av faktisk og eller ønsket vekst-rate.

Uten dette instrumentet, og med troen på at man snart, kun innen noen måneder med
hestekur kan komme tilbake til positiv økonomisk vekst, så vil man oppleve at

- Nei, det ble visst ikke bare et kort intermesso med hestekur, det ble visst en
  skikkelig økonomisk kollaps, gitt.
- Alle skyldte på alle andre, innvandring, at publikum ikke forsto at de måtte
  ut å handle for å få opp veksten igjen, at bankene var problemet fordi de hadde
  lånt ut penger til for risikable prosjekter, at de fattigste var problemet, for
  de gadd jo ikke jobbe, men bare Nave, at de rikeste var problemet, for de
  tenkte jo bare på å redde seg selv, at nabostaten var problemet, for de startet
  med å oppføre seg aggresivt når myndighetene innså at det var vanskelig å innfri
  valgløfter, at dette ikke kunne fikses kun på eget territorium
- Kollapsen ble så grunnleggende, og ingen forsto riktig hvorfor ikke det gikk,
  så folk ble så desperate når ingen kunne forklare hva som skjedde at folk ikke
  så noen annen utvei enn vold og våpen nå som alt likevel gikk over styr.

Grunnen til at alt kollapset var jo at altfor mange faktisk trodde
at det var mulig å få til fortsatt økonomisk vekst, og at frustrasjonen
over ikke å få det til førte galt avsted.

Det verste ved det hele, er at etter at krigen har endt, når det er på tide
å bygge seg opp av ruinene igjen, så blir det ikke tilstrekkelig mange som
husker eller vet at kollapsen faktisk *hadde* sammenheng med
bestrebelsene på økonomisk vekst. Nå er man jo bombet så langt tilbake
at det på nytt er nødvendig med ny økonomisk vekst, og så går det omtrent
100 år til før det neste gang blir et problem at veksten ikke kan fortsette.

Stikkord som ettehert skal utdypes:

- Renteproblemet, at det kan tenkes at man ikke får nok K-renter inn i banken for å dekke M-renten,
  vise hvordan dette kan løses. (Litt av kvantitativ-lette-pengene kan brukes til å dekke M-renten)
- F = (M - K) vil være lik akkumulerte fiatpenger over alle år praksisen har blitt utført
- maks fiatpenger pr. år kan settes til en prosentsats av M, f. eks. 6%
- maks tillatt K kan settes til en prosentsats av A
- f. eks 100% (at bankene kan doble pengemengden hva. kreditt)
- eller 20% (at banken kun kan utvide pengemengden litt hva. kreditt)
- Argumentere for at kreditt-kort ikke hører hjemme i et system der M > K
- Huske på at gryn-justert rente i et system med pengemengde-utvanning på 6% pr. år i praksis er
  negativ helt opp til at krone-renten er 6%.
- Innskuddsrenten bør alltid være lavere enn pengemengde-utvanningen. Dermed sikrer man seg at
  pengene blir mindre verdt med tiden
- Innskuddrenten over et visst beløp bør være **lavere** enn på de første, si, 100000 kronene,
  ikke høyere som er vanlig praksis nå. De som har mye penger bør få et incitament til å investere dem
  De som har lite penger er de som har mest bruk for renter.
- Det er ikke noe galt i at gryn-justert rente for utlån også er negativ, skulle jeg tro,
  jeg mener, systemet bryter ikke sammen av den grunn.
- Vise til at dette var en diskusjon under depresjonen på 1930-tallet, der de som
  nektet å starte denne praksisen vant og at det var noe av forklaringen på at
  alt raknet. (Hvor leste jeg nå dette?)
- Vise likheten mellom et slikt system og Charles Eisensteins forslag om å innføre
  negativ rente.
- Erkjenne at det kan tenkes at penger i et slik system kan hope seg opp hos noen
  få samfunnsborgere, og at man må sjekke dette gjennom teoretiske modeller der man
  (hvis det er et problem, da) setter inn reguleringer for å motvirke dette
- Vise at pengemengden kan vokse eksponensielt selvom real-økonomien ikke trenger vokse
- Vise at pengemengden faktisk *kan* vokse eksponensielt, som med kølapper som av
  og til folder tilbake til null (en lineær prosess riktignok), så kan pengemengden
  brutalt justeres nedover ved å stryke et par tre nuller og kalle penge-enheten
  noe nytt, omtrent som når Italienerene gikk fra Lire til Euro. Ingen dramatikk,
  bare nye tall og forholde seg til for alle.
- Vise at en tilsvarende reform i real-økonomien ikke er det samme. Å gå løs på
  huset sitt med motorsag slik at kun én prosent av det står igjen, det merker du,
  selvom alle naboene gjør det samme.
- **Krig** er en høyst aktuell måte å foreta en slik
  "justering" på.

Private pengesystemer
~~~~~~~~~~~~~~~~~~~~~

- Vise at LETS ikke funker alene fordi den mangler skaléring. Den offisielle valutaen brukes som
  skalerings-faktor.
- Ta utgangspunkt i LETS og vise hva som skjer dersom alle i utgangspunktet får en positiv offset
  i stedet for 0. Svar: ingenting annet en at alle tallene på kontoene får denne offseten og at
  alle tallene derfor kan være positive.


Mulig at alt dette skal slettes eller flyttes til andre steder [#99]_



.. Frivillig arbeid [#99]_
   binder opp penger
   Folk får mindre tid fordi de er opptatt med å forbruke
   [#99]_ Eksponensiell ve  kst, hvorfor er det mulig med penger men ikke med real-økonomi


Jeg tror at mye av grunnen til at

- Fra gjelds-basert til en kombinasjon av "frie penger" og gjeldsbaserte penger
- Med "frie penger" mener jeg penger som har fremkommet vha. uttynning av pengemengden
  dvs. i praksis det samme som skatt på penger

.. [#99]_ Dette må skrives om

Konkretisere hva penger er. Gullstandard. Enkelte økonomer vil tilbake til gullstandard!
Hva tenker de på?

Likevel, det er viktig at penger konkretiseres, at vi har en tydelig forståelse av hva penger er.
Slik de fleste opplever samfunnet nå, så har de en tilstrekkelig forståelse for hvordan penger
virker til at de kan styre sin egen økonomi sånn nogenlunde.
Men vet flerallet hvordan penger fungere på litt lenger sikt?
Er det i det hele tatt enkelt å få oversikt over dette?
Er det så komplekst at det må overlates til økonomer, og at vi ikke trenger fundere over dette
selv, for økonomene har jo full kontroll?
Har økonomene full kontroll?
Ta Urix 19.11.2014 bl. a. om de økonomiske utsiktene for Euro-området med Hege Moe Eriksen med
bl. a. Josph Stiglitz Kjersti Haugland seniorøkonom DnB-markets og Harald Magnus Andersen,
sjefsøkonom Swedbank, der økonomene mente at veksten i Eurosonen vil ta seg opp,
men det vil nok ta et par år, så det ser mørkt ut nå, men vi må se optimistisk på det.
Kom det fram noen konkrete synspunkter som virkelig belyste og klargjorde hvorfor den økonomiske
veksten i Euro-sonen kommer til å ta seg opp?
Jeg synes ikke det.
Eller ta en bok om penger som jeg ble tipset om av Per Hjalmar Svae, forfatteren av "Løsningen er grønn" [#SvaeGronn]_.
Foranledningen for hans tips var en samtale vi hadde etter et foredrag han holdt om boken sin.
Jeg trakk fram endel bøker og forfattere jeg synes om,
og at jeg så et behov for å reformere hele pengesystemet.
Det er særlig forfatteren Silvio Gesell som hjulpet meg i å forstå hva penger egentlig er.
Per Hjalmar Svae viste en viss interess for dette, men var skepisk (det bør man jo være!).
Ved neste anledning vi traff hverandre hadde han funnet en bok som er basert på tanken til Silvio Gesell:
Sacred Economics av Charles Eisenstein.
Jeg kjøpte boken og leste den.
Eisensteins konklusjoner er jeg ikke helt enig i (negativ rente),
selvom dette kan være en farbar vei for å oppnå økonomisk stabilitet selv uten økonomisk vekst.
Det er appendixet i boka jeg synes er mest interessant.
Her drodler Eisenstein over hvor vanskelig det egentlig er å fatte hva penger er,
selv om man bruker lang tid på å prøve å forstå det.

I det hele tatt,
jeg savner en god begrunnelse for hvorfor det kan la seg gjøre å fortsatt øke forbruket i vår del av verden.
Det blir framhevet at vekst er nødvendig for at de svakeste gruppene skal komme seg opp,
og det høres fornuftig ut,
men mange mener at vi heller må se på problemet med ulikheter at vi bør fordele de godene som finnes bedre.

Full-reserve vs. fractional reserve, som beskrevet av Eisenstein, en diskusjon rundt dette [#99]_

Gjeld [#99]_
------------

- Internasjonal gjeld, diskutere at den bør ha en tidsbegrensning, en dø-ut-av-seg-selv-faktor fordi
  en ikke bør kunne tillate at én generasjon av befolkningen kan påføre neste generasjon uoverkommelige
  mengder med gjeld til utlandet
- Diskutere hva som skjer dersom en låntaker misligholder lånet sitt.
- Hvem betaler tilbake lånet? Det kan være alle samfunnsmedlemmene som har penger, eller alle
  samfunnsmedlemmene som har lån eller bare de som har aksjer i den banken som lånet står i eller
  en kombinasjon

Fond (flytt dette) [#99]_
-------------------------

- Minne på at oljefondet vårt like gjerne kan skrumpe inn og fordufte, slik er nå engang regler om
  aksjer og fond.
- Vise at hvis oljefondet dør på seg, så betyr det at vi i realiteten har solgt olje til utlandet
  uten å få den prisen som vi tilsynelatende fikk, vi har jobbet tildels gratis for utlandet.

.. [#NorgesBankFinansmarkeder] Norges Bank Skriftserie nr. 34, 2004
.. [#Piketty] Thomas Piketty, Kapitalen i det 21. århundre, 2013
.. [#LarsenPenger] Erlig Røed Larsen - Penger, 2012
.. [#EisensteinSacredEconomics] Charles Eisenstein, Sacred Economics, 2011
.. [#IslandskPengeReform] Frosti Sigurjónsson, Monetary Reform - A better monetary system for Iceland
.. [#GronnSosialokonomi] Erik Grønn, Samfunnsøkonomiske emner, 2002
.. [#RingstadMakro] Vidar Ringstad, Makroøkonomi og norsk stabiliseringspolitikk, 2001
.. [#Gesell] Silvio Gesell, The Natural Economic Order, 1918
   http://www.community-exchange.org/docs/Gesell/en/neo/

.. [#JohansenStenEuro] Øystein Kock Johansen, Fra stenpenger til Euro, 2001
.. [#MethodLoci] http://en.wikipedia.org/wiki/Method_of_loci
       **Method of loci** eller **Minne-palass** Metode for å huske store mengder informasjon

.. [#LETS] http://en.wikipedia.org/wiki/Local_exchange_trading_system
.. [#GlobalePenger] Arne Jon Isachsen og Geir Bjønnes Høidal, Globale penger, 2004
.. [#SvaeGronn] Per hj. Svae, Løsningen er grønn, 2013
.. [#TrondAndresenFB] https://www.facebook.com/trond.andresen
.. [#HysjViRegner] Roman Linneberg Eliassen, Hysj, vi regner, 2016
.. [#DebtDevil] Adair Turner, Between Debt and the Devil, 2016
