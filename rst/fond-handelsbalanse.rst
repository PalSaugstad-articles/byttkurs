Aksjer og fond og handelsbalanse
================================

- Vise hvordan fond, f. eks. pensjonsfond (innland) klusser til tidsbegrepet
- Vise at realøkonomien fungerer der og da, man kan ikke spare på real-økonomi
  mens man er i arbeidsfør alder, og så slippe det løs når man er pensjonist.
- Solidaritet mellom generasjoner som alternativ til pensjonsfond
- Vise hvordan oljefondet klusser til tiden (sylte ned midler for å oppretthold
  arbeidsplasser nå, overføre latent arbeidsløshet til framtidige generasjoner)
- Sannsynliggjøre at fond ikke kan vokse stort mer, og ikke kan vokse med mindre
  hele real-økonomien vokser
- Vise at navnet "Pensjonsfond utland" er høyst tvilsomt (vekslings-problematikken),
- alternativt at pensjonister må lands-forvises å slå seg ned i et dollar-valuta-område
- alternativt at pensjonister må leve innenfor en dollar-sone i Norge hvor
  alle tjeneste-ytere blir betalt i dollar og ikke får leve sine liv her, men må
  pendle fra dollar-land for å jobbe i Norge.
- Referanser til "Petromania" av Simen Sætre [#Petromania]_
- Generell problematikk rundt det å være netto-eksportør, man binder arbeidskraft
  til å jobbe med noe som kun gir et potensielt overskudd på handelsbalansen,
  denne arbeidskraften kan ikke brukes til å bygge hjem-landet direkte, kun evt. indirekte
  via motytelser fra utlandet.
- Se dette i lys av at vi i Norge ikke har mye bedre standard på skolebygg, svømmebasseng
  osv. (enn Danmark/Sverige f. eks.) selvom vi flommer over av utenlandsformue,
  og at dette har sin naturlige forklaring
- [#99]_ hører ikke hjemme her men er viktig: hm, hva var det jeg tenkte på?
  Offentlig sektors størrelse (helsevesen f. eks.) i en økonomi som vokser og vise
  at prosesser som for det meste innbefatter tjenester nødvendigvis må bli relativt
  sett dyrere når den materelle levestandarden vokser fordi materielle ting blir relativt
  sett billigere (direkte målbart i gryn)
- [#99]_ Diskusjon rundt minste-lønn, vise at man må se minste-lønn, forbruk og produktivitet
  i sammenheng. Å avskaffe minste-lønn, ikke ta hensyn til produktivitet (at den er for høy)
  ikke ta hensyn til at forbruket og dermed økonimisk vekst kan stoppe pga. metnings-problematikken,
  bidrar sterkt til forverring av Gini-indeksen

.. [#Petromania] Simen Sætre, Petromania, 2009 http://www.adlibris.com/no/bok/petromania-9788272014406