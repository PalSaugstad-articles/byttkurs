Indekser
========

Konsumprisindeks
----------------

Konsumprisindeksen er en problematisk størrelse, synes jeg.
Idéen med den er å gjøre forskjellige tider (år) sammenliknbare med hverandre
ved å eliminere virkningen av pris-endring.
For å få til dette har man definert seg en handlekurv med en viss mengde
av et visst antall varer i.
KPI bestemmes deretter av endringen i pris i kroner av varene i kurven.

Det er flere problemer her. Mengden varer og vareutvalget må gjøres med
skjønn. I historisk perspektiv *må* man bytte ut varer i kurven fordi
enkelte varer fases ut av real-økonomien og nye varer fases inn. KPI
over lange tidsintervaller blir derfor ganske unøyaktige.

En annen ting er at man ikke kan legge hus ned i den kurven.
Ikke fordi kurven er for liten til å få plass til et hus, vi snakker jo her om en tenkt kurv,
men fordi beregningen av KPI ville bite seg selv i halen dersom man la huspriser
ned i den.
Det skyldes at for å kjøpe seg hus må man ta opp lån (kreditt), og dette er en av de
kraftigste motorene for å øke pengemengden, og pengemengden er i sin tur
én av faktorene som bestemmer
prisutviklingen til alle varer (også husprisene).

KPI brukes jo bl.a. for å justere BNP slik at en kan beregne real-veksten i økonomien.
Hvis KPI ikke er særlig nøyaktig over et stort tidsintervall, så vil jo ikke
BNP-sammenlikninger over lang tid være det heller.

BNP/BNI [#99]_ 
~~~~~~~~~~~~~~

- Diskutere fordeler og ulemper med BNP/BNI-konseptet
- Heller ta utgangspunkt i forbruks-siden enn produksjons-siden
- Vise forskjellen mellom gjennomstrømning av varer/tjenester og den statiske størrelsen/mengden av varer/tjenester
- Vise at en 1-gryning i forskjellige valuta-områder ikke nødvendigvis er like rike selvom valuta-mengdene er eliminert
- Finne ut om KPI i gryn kan brukes som faktor for å eliminere den gjennomsnittlige forksjellen i
  materiell velstand mellom valutaområder
- Foreslå å bruke denne faktoren som alternativ til BNP
- Minne på at holbarheten av ting vil påvirke BNP og den nye faktoren forskjellig gitt samme komfort .. eller, kanskje ikke?
  fordi hvis ting har en gitt holdbarhet men at holdbarheten på den nye generasjonen av samme tingen får
  høyere holdbarhet (uten av produksjons-innsatsen øker i samme grad, la oss si, ikke i det hele tatt),
  så vil BNP gå ned mens den nye faktoren ikke vil endre seg
  (hvis vi ser bort fra at hele økonomien endrer nivå når man innfører den nye generasjonen fordi dette
  potensielt sett påvirker sysselsettingen negativt).
- BNP er en faktor av type gjennomstrømning mens den nye faktoren er av typen størrelse/mengde
- [#99]_ Hører hjemme et annet sted, men sånn apropos lenger holdbarhet:
  Hvordan analysere lenger holdbarhet, forskyvning av skatt fra arbeid til energi, lavere produktivitet, mindre behov for transport?
  I dette bildet synes jeg man kan se en positiv og tenkelig utvikling i et system med negativ økonomisk vekst som
  folk flest vil synes er positivt.
  Man kan erstatte industrialiert la os si møbelproduksjon der man brutalt henter råvarer som
  trevirke osv. og dytter det inn i upersonlige maskiner som ikke nødvendigvis utnytter trevirket på best mulig måte og får
  som resultat likegyldige møbler etter bruk og bytt/kast-prinsippet til "uproduktiv" produksjon der trevirket blir behandlet
  av håndverkere som liker tanken på å utnytte resursene optimalt med resultat at man får et møbel som man vet mere om
  og som har et emosjonelt aspekt ved seg og som man av den grunn tar bedre vare på.
- I "det 5. trinn" [#Andersen5]_ terminorogi: den emosjonelle materien innbakt i den fysiske materien eller noe slikt antakelig.
- [#99]_ annet apropos som skal flyttes: Klima vs. vekst som Naomi Klein beskriver i sin siste bok [#KleinChanges]_\ . Det belyser hvor
  nødvendig det er å diskutere en framtid uten økonomisk vekst.
  Klimatoppmøtet i Paris desember 2015 kommer til å bli samme
  gamle bedrøvelsige historie som alltid dersom ikke negativ økonomisk vekst settes på dagsorden snarest!

Human Development Index
-----------------------

Noen få målepunkter i samfunnet er her brukt for å sette sammen en index
for å kunne sammenlikne verdens land.

HDI
~~~

- Forventet levealder
- Gjennomsnitlig antall skoleår
- Bruttonasjonalinntekt

IHDI
~~~~

Som HDI, men ulikhets-justert (Gini-index)

Problem med HDI/IHDI
~~~~~~~~~~~~~~~~~~~~

Det er særlig Bruttonasjonalinntekt som er problematisk her etter min mening,
fordi det forutsetter at man må regne i en gitt valuta (Dollar), og KPI må tas i betrakning.
Det ville vært bedre om valuta kunne holdes utenom [#99]_


Forslag til en alternativ indeks
--------------------------------

- Energi-forbruk pr. innbygger
- (Energi-effektivitet)
- Andel av befolkningen som er innesperret/bortført/myrdet
- Andel av befolkningen som synger i sangkor
- Gini index


Energiforbruk
~~~~~~~~~~~~~

Jeg mener at det er en sammenheng mellom økonomisk vekst og vekst i energiforbruket.
Grunnen til det er at produksjon av varer og tjenester krever energi.
Energiforbruk kan måles i kWh, Kilowatt-timer, uansett hvilken energitype vi tar utgangpunkt i.
Man kan si at forskjellige klimatiske forhold vil ha en effekt på grunn-forbruket,
eksempelvis har man regnet Norden for å være i en særstilling siden det er så kaldt her
at vi trenger mye energi til oppvarming.
Saken er at det faktisk er lettere å bruke lite energi i et kaldt klima (innen rimelighetens grenser)
enn i et varmt klima der energien brukes til *nedkjøling*.
I kaldt klima kan man stort sett isolere ute kulden (eller rettere sagt isolere varmen inne),
og bruke restvarmen fra mennesker og annet energikrevende aktivitet som eneste oppvarming.
I et varmt miljø, derimot, kan man ikke unngå å utvikle varme innendørs,
og dette må kjøles bort dersom man vil holde lik eller lavere temperatur inne enn ute.
For å kjøle aktivt ned er man nødt til å bruke energi.
Man kan riktignok kjøle ned passivt ved at man utnytter lavere nattetemperatur
og utstråling av varme om natten.
Det er antakelig omtrent like omfattende å bygge om bolig- og næringsbygg-bygningsmassen
i varme strøk til å utnytte kalde netter for nedkjøling
som det er å bygge om bygningsmassen i kalde strøk til å ta vare på varmen.

Sammensetningen av den energien man bruker bør ikke påvirke målingen.
Med det mener jeg at vind-energi så vel som atom-kraft eller kullkraft skal telle like mye,
kun bestemt av energimengden målt i kWh.

Det kan diskuteres om man skal regne med uønsket spillvarme eller ikke i forbruket:
Ta framstilling av elektrisk energi.
En typisk vannkraftverk utnytter mer enn 90% av den potensielle energien i vannet,
og det blir tilsvarende lite spillvarme.
Et kullkraftverk vil ha en typisk virkningsgrad på mellom 40% og 47%,
det vil si at 53% til 60% av energien i kullet blir borte i spillvarme
som man riktignok *kan* utnytte dersom det finnes industri eller boliger i nærheten som trenger varme,
men mye av denne spillvarmen blir ikke utnyttet i praksis.
En mulighet er å bare regne "ferdigprodusert energi",
dvs. se bort fra spillvarmen dersom den ikke utnyttes.
I så fall bør man i tillegg innføre en generell virkningsgrad-indeks for energisektoren
som inkluderer alle uønskede prosess-tap,
det være seg i varmekraftverk, petroliums-raffenerier,
vannkraft (tap i rørgater, generatorer osv.), vindkraft osv.

En kanskje like bra tilnærming er å ikke operere med en effektivitets-indeks,
men regne hele den opprinnelige energimengden med til forbruket.
Det vil i praksis si at dersom du trenger 24 kWt i døgnet i energi i hjemmet ditt
og du får levert alt dette som elektrisk energi,
så trenger du noe slikt som 26,7 kWh vann-energi fra et vannkraftverk med 90% virkningsgrad,
mens du trenger 60 kWh kull-energi fra et kullkraftverk med 40% virkningsgrad.

For land som produserer varer for eksport vil det også være en diskusjon rundt hvem
som skal belastes for energiforbruket som produksjon av disse varene representerer.
Det mest rettferdige vil vel være om det var forbrukerene av disse varene
som ble belastet dette energiforbruket.
Da må man holde styr på all handel med varer og dessuten knytte en forbrukt energimengde med hver vare.
I slike beregninger vil det antakelig oppstå en god del unøyaktigheter.
Dersom det lar seg gjøre å beregne dette uten for mye feil,
vil dette antakelig gi det beste bildet på reelt energiforbruk.
Alternativt kan man regne all forbrukt energi,
også til eksportindustri som forbruk i det landet produksjonen foregår.
Dette er mye enklere å beregne.
Dessuten er det jo slik at i en velfungerende verdenshandel
må alle land sørge for å ha balanse i sin utenrikshandel:
Dersom et land over lang tid importerer mer enn det eksporterer
(målt i pengeverdier, riktignok, ikke energi-ekvivalenter),
så skaffer landet seg en gjeld overfor utlandet.
Motsatt, så vil et land som eksporterer mer enn det importerer skaffe seg en utenlands-formue.
Mer om dette i et senere kapittel [#99]_.
Den siste måten å regne på kan også være nyttig dersom, hm [#99]_.

Den energi-produserende industrien er i en særstilling.
Eksportert energi må jo regnes som forbrukt der den faktisk blir forbrukt,
ikke der den er produsert uansett hvilket av alternativene over man velger.
Riktignok så bruker energi-industrien også energi (dvs. tapt energi).
De to beregningsmåtene vil regne denne tapte energien enten som en belasting for
det området som forbruker energien, eller for det området som produserer energien.

Problemet her er jo mangelen på balanse i utenriks-regnskapet som de fleste energi-ekporterende
land opplever. Mer om dette i et senere kapittel [#99]_

Andel av befolkningen som er innesperret/bortført/myrdet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Jeg tror mange samfunnsparametere er iboende i denne parameteren.
Man kan i utgangpunktet anta at et samfunn der en liten andel av befolkningen sitter i fengsel
eller blir myrdet er et trygt samfunn.

Hvordan beregne denne?
^^^^^^^^^^^^^^^^^^^^^^

Vi har, fengsede, bortførte, henrettede og myrdete,
og vi bør ta i betrakning både sivilsamfunnet generelt,
samt hendelser som skjer i forbindelse med kriger og konflikter.
Det er to måter å regne på, fataliteter pr. år pr. 1000 innbyggere,
eller antall "satt ut av spill" pr. 1000 innbyggere.
Med fatalitet mener jeg tidspunktet for fengsling, bortføring, henrettelse eller mord.
Med "satt ut av spill" mener jeg frarøvet et liv i frihet.
Den siste metoden er antakelig enklest å beregne.
For det første alternativet er det fengslinger som er problemet:
Samme person kan ha mange korte straffer, og hver gang han fengsles vil da telle.
For det andre alternativet er det henrettelser og mord som er problematisk,
for man må sette en grense for når en henrettet/myrdet person likevel ville ha dødd en naturlig død:
La oss si vi setter forventet alder til 70 år.
Da vil en 60-åring som henrettes, bortføres uten noen gang å komme til rette
eller myrdes regnes med i statistikken over de som er satt ut av spill i de ti neste årene.
Hvis man for eksempel skal regne denne parameteren for Norge i 2015, så vil antallet som er myrdet
bli berenget ved å se på hvor mange som er myrdet de siste 70 årene
fratrukket de som er født før 1945.

Denne verdien kombinerer (minst) to hoved-scenarioer som begge sier noe om hvor trygg man føler seg.
I et samfunn der myndighetene er autoritære kan vi tenke oss at fengsliger og henrettelser
forekommer oftere enn nødvendig/ønskelig.
I et samfunn med dårlig retts-sikkerhet og et lite utviklet politi-apparat
kan vi tenke oss at antall bortføringer og mord er høye.
Hvis det hverken er mange fengslede/henrettede, mange bortførte eller mange myrdete
kan man anta at hele samfunnet fungerer bra når det gjelder kriminalitet og rettsikkerhet generelt.
Man kan innvende at mindre alvorlig kriminalitet enn mord også går på trygghetsfølelsen,
men av kriminalitet så er det de nevnte tilfellene som er lettest å lage statistikk for,
og man kan anta at mindre alvorlig kriminalitet forekommer proporsjonalt med de målte verdiene.

Andel av befolkningen som synger i sangkor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Er dette litt vel oppfinnsomt? [#99]_
Poenget er vel å finne en kulturell aktivitet som er forholdsvis universell.

Denne verdien sier noe om livskvaliteten i et samfunn.
Det å synge i sangkor, og det å lytte til et sangkor tror jeg aldri vil gå av moten.
Det er en aktivitet som vi kjenner fra de fleste samfunn som er dokumentert,
og man er ikke avhengig av ytre resurser for å danne et sangkor,
så denne parameteren er nøytral med tanke på materielle levestandard.
Parameteren er også forholdsvis lett å måle.

Gini-index
~~~~~~~~~~

Gini-indexen, altså inntektsfordeling bla.bla

Andre parametere
~~~~~~~~~~~~~~~~

Vil det ikke være hensiktmessig å ta med andre parametere? Hva med utdannelse?
Min mening er at det denne parameteren *kunne* vært tatt med,
men at man kan anta at det er en viss sammenheng mellom de parameterene som er i bruk og utdannelse,
som f. eks. at det er mer sannsynlig at man har en lav utdannelses-andel dersom
man har mange i "satt ut av spill".

Sammensetning av disse parameterene til én index
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

HDI-indeken er laget slik at de forskjellige elementene i dem blir vektlagt
slik at alle verdiene kan slås sammen til én index.
Det er diskusjoner rundt hvordan de tre komponentene i HDI skal vektlegges.

Når det gjelder de fire (fem) parameterene jeg foreslår her,
så må man også for disse finne ut hvordan de skal veies
før man evt. slår dem sammen til én indeks.

Hvordan skal f. eks. energiforbruket pr. innbygger vektlegges?
Forskjellige grupperinger i befolkningen vil være uenig i hva som er "riktig" energiforbruk pr. innbygger.
De som ønsker at vi skal ha kontinuerlig økonomisk vekst vil vel ha det synspunktet at
*absolutt-verdien* for energiforbruk pr. innbygger er irrelevant,
men at *den årlige veksten* i energiforbruk pr. innbygger bør være eksempelvis mellom 1% og 3%.
For denne gruppen vil vektleggingen av energiforbruket være at høyere forbruk er bedre,
og dette tilsvarer i grove trekk bruttonasjonal-intekts-faktoren i HDI-indeksen.

Noen "vekst-talsmenn" vil vel betvile
at det er noen direkte sammenheng mellom økonomisk vekst og energi-forbruk,
og vil muligens se denne parameteren som helt uinteressant.

En annen gruppe er klima-aktivistene, som ser på jobb nr. én som det å få ned CO\ :sub:`2`-utslippet.
Det finnes vel antakelig to undergrupper av disse,
de som mener at vi må ta ibruk fornybar energi i mye større skala som erstattning for fossil-kraft,
men uten å ha noen definert målsetting for total-forbruket.
Den andre undergruppen erkjenner at energi er konverterbar og kan dermed brukes i flere sammenhenger,
slik at det både er et poeng å øke fornybar energi, og å få ned total-forbruket.
For denne gruppen vil vektleggingen av energiforbruket typisk bli at lavere forbruk skal gi høyere score [#99]_.
Det er mulig at man vil gå inn for en ideal-verdi,
hvis man kan anta at det finnes et energiforbruk som er så lavt at livs-kvaliteten uten tvil lider.

Uten å diskutere i detalj hvordan de andre parameterene kan vektlegges,
vil jeg si at det kanskje ikke er lurt å slå alt sammen i én indeks.
Dette gjelder også den etablerte HDI-indeksen:
Når man sidestiller forventet levealder, utdannelses-nivå og bruttonasjonalinntekt,
hva gjør man egentlig da?
Det vil altså si at det ikke er så farlig med levealderen,
det kan kompenseres med et høyere utdannelsesnivå eller høyere nasjonalprodukt!
Det blir vel omtrent som at meteorologene sluttet å melde nedbør og vind og temperatur
som forskjellige værfenomener.
De kan bli enige om en vektlegging som går fra fin-vær til dårlig-vær der alle ligger inne.
Meteorologen: "I morgen blir været dessverre bare terningkast 3.
Mer kan jeg ikke si."

.. [#KleinChanges] Naomi Klein, This Changes Everything, 2014
