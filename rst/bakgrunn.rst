.. [#99]_ Bør antakeligvis deles opp

Bakgrunn
========

Dag Andersen, det 5. trinn [#Andersen5]_

Dag Andersen ga ut en bok i 2003 der han beskriver historiens gang på en tankevekkende måte.
Han deler opp den menneskelige historien i utviklingstrinn, og sannsynliggjør
at vi fra det trinnet vi er nå, det 4. trinnet,
kommer til å klatre opp på neste trinn,
det 5. uten at vi med sikkehet kan si hva 5. trinn innebærer.
Hvis man ser bakover i historien kan man,
sier han,
identifisere trinn i form av den første bosetnings/dyrkings-perioden,
middelalderen,
beherskelsen av det fysiske.
Det vi ikke behersker fullt ut ennå,
er prosessene i vår mentale verden.
Én av tingene som skal til for å komme til neste trinn,
tror jeg,
er å forstå økonomiske prosesser bedre.
Økonomifaget har utviklet seg,
og tildels pendlet fram og tilbake mellom forskjellige virkelighets-oppfatninger.
Det er vanskelig å vite hva neste trinn vil innebære,
men noe som er "et salig rot" nå,
og som folk flest helst ikke vil vite noe om, er makro-økonomi.
Etter min mening (og også Thomas Piketty sin mening tror jeg)
er det en stor svakhet at ikke flere engasjerer seg i makroøkonomi.

Jeg ble interessert i feltet ved en ren tilfeldighet.
Jeg husker øyeblikket ganske godt.
Før dette var jeg også blant de mange som tenkte at økonomi og makroøkonomi,
det bør vi overlate til økonomene og politikerene,
så får jeg som teknolog holde på med mitt, dvs. utvikle nye produkter.
Så hva skjedde den dagen for over 10 år siden når min interesse ble vekket?
Rett og slett grubling.
Jeg satt på kontorstolen min på jobben og begynte (nok en gang..) å gruble over hva
drivkreftene bak varierende priser på varer egentlig var.
Jeg visste jo godt hva inflasjon var og reallønnsøkning og alt det der.
Jeg hadde vel også en brukbart bra formening om hva økonomisk vekst var,
noe jeg var skeptisk til med den begrunnelsen at økonomisk aktivitet kan da ikke øke i det uendelige,
og at økonomisk vekst og økt energiforbruk hører sammen,
så hvis man virkelig ville begrense CO\ :sub:`2`-utslipp og andre negative miljø-påvirkninger,
så burde man vel i alle fall diskutere i hvor stor grad man fortsatt skulle gå inn for økonomisk vekst,
og kanskje i alle fall gå inn for nullvekst.

Svak bærekraft, sterk bærekraft [#99]_, side 74 i [#SvaeGronn]_

Men tilbake til min grubling den dagen:
Ta for eksempel Krone-is.
Når jeg var barn kostet den faktisk en krone, men nå var den kommet opp i 13 kr.
Greitt nok egentlig, det har noen med vekst og de greiene der å gjøre,
men så kom jeg til å fundere over makro-elementet i dette:
Hvis Krone-is "idag" koster 13 ganger mer enn da jeg var barn,
og alle andre varer og tjenester hadde økt omtrent det samme i tilsvarende periode,
da måtte det enten være slik at antall kroner i omløp,
og dermed hvor mange kroner som eksisterte før i forhold til nå måtte ha økt tilsvarende,
eller at det lå en hel haug med penger rundt å slang,
i et bankhvelv eller på en ekstra-konto eller noe når jeg var barn som etter hvert hadde kommet i bruk,
og i så fall,
var det snart tomt for penger som ikke var i bruk,
og hvis det var tilfelle,
ville det bety at prisene og lønningene umulig kunne øke mer?
Jeg hadde i alle fall ikke lagt merke til noen nyhet noen gang om at Stortinget hadde bestemt
seg for at vi trengte mer penger og derfor hadde besluttet å generere noen fler.
Såpass lite visste jeg om makro-økonomi på den tiden.
Nå, etter å ha sjekket litt, lest litt og fulgt med litt vil jeg si at jeg vet endel mer,
men for all del, jeg er ikke økonom,
har ingen formell økonomisk utdannelse bortsett fra et fag på universitetet som het
samfunnsøkonomi [#99]_ (sjekk vitnemål) som jeg fikk laveste stå-karakter i.
Ikke noe gunstig utgangpunkt for å ville mene såpass mye om et så stort felt at man gir seg
ikast med å lage en bok (eller lite hefte?) om det.
Men på den andre siden,
det finnes jo skoler innen økonomi-faget som kan ha stikk motsatte meninger om samme tema:
Ta f. eks. økonomisk vekst og hvordan Mark Thornton med sitt utgangspunkt,
Mises Institute "Den østerikske skole" er totalt uenig i hva som er utgangspunktet,
drivkreftene bak, økonomisk vekst i forhold til de fleste andre økonomiske retninger.

Thomas Piketty har jo laget en murstein av et økonomisk verk, Kapitalen i det 21. århundre [#Piketty]_.
Han kommer ikke med noen facit-svar på hvor bør vi gå herfra,
og innrømmer at samfunnsfag generelt bygger på et svakt grunnlag,
og at han derfor har forhåpninger om at det kan gjøres store framskritt,
og at også ikke-fag-personer bør engasjere seg.


Det fine med å være lekmann sånn som meg,
er at man kan drodle friere over temaer enn en fagperson kan gjøre:
Hvorfor? En fagperson som "stikker hodet ut" kan risikere å bli helt avvist av sitt miljø,
og det kan være en risiko mange ikke vil ta.
I et fagmiljø vil det også antakelig danne seg en slags uuttalt forståelse,
en indoktrinering som man ikke klarer å bryte med.
Det at det danner seg klikker, Nyliberalisme, Keynesianisme, Østerikske skole,
tyder vel også på at det finnes en slags indoktrinerings-effekt i slike fagmiljøer.

Jeg tror indoktrinering er et nøkkelord:
Det man selv i sitt eget miljø ser på som helt naturlig,
vil av andre fortone seg som meninger framkommet ved grov indoktrinering.
Jeg mener at den blinde troen på økonomisk vekst skyldes en slik indoktrinering:
Alle har snakket om behovet for økonomisk vekst så lenge at ingen lenger klarer å tenke seg
en framtid uten.

