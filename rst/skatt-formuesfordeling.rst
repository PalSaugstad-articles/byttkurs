Skatt, utjevning av formues-fordeling
=====================================

Jeg forutsetter her at pengesystemet er reformert slik at M vokser og at K skal være
mindre enn M slik at også svak økonomisk negativ vekst er mulig.

- Redusere skatt på arbeid og øke skatt på energi
- Se [#SvaeGronn]_ side 160 til 167 og tabell side 150. Her er riktignok bare energi
  som genererer CO\ :sub:`2` diskutert [#99]_ 
- Skatt på energi kan helt erstatte mva, for dette vil gi en automatisk skattefordel
  dersom man klarer å produsere uten stor energi-tilførsel
- skatt på energi vil gjøre at arbeidskraft i større grad blir foretrukket framfor
  automatisering, og dermed bidra til lavere produktivitet og dermed lavere arbeidsløshet,
  mao. dra gini-indexen ned.
- Skatten på energi bør være beregnet på grunnlag av energi-innholdet i den opprinnelige
  energien, slik at tapet i produksjons-prosessen fører til at man foretrekker
  energi som trenger liten energi-innsats.
- Man kan tenke seg kun to primære beskatnings-områder: Energi og penger (via
  pengemengdeutvanning)
- Antakelig ikke mulig å redusere antall super-rike vha. progressiv beskatning alene,
  fordi super-rike som regel tjener penger på noe et veldig stort antall mennesker vil ha,
  slik at økt skatt for den super-rike kan "faktureres ut" i form av litt dyrere varer og
  tjenester.
  
Formuesutjevning i et fungerende system med svak negativ økonomisk vekst
------------------------------------------------------------------------

- Ved svak negativ økonomisk vekst vil interessen for aksjer og fond synke fordi
  man ikke lenger vil oppleve formues-økning i form av eierskap. (Dette er en hypotese
  fra min side, jeg vet ikke om det finnes litteratur om det). (Dette må sjekkes)
- Kooperasjoner som eierskap og etablering fungerer bedre i et slikt miljø (hypotese)
- En mulig måte å få til utjevning er å sette real-økonomiske grenser, at
  man ikke har lov til å tilegne seg mer land (f. eks.) enn en viss mengde (det vil
  bli meget sterk motstand mot et slikt forslag, skulle jeg tro)
